﻿namespace TrafficClassification
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox_resultOut = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.textBox_percentSplit = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.label13 = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.textBox_numbTraining = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.features = new System.Windows.Forms.CheckedListBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox_type_classification = new System.Windows.Forms.ComboBox();
            this.Name_DB_creating = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.comboBox_type_DB = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.textBox_Name_Exp_1 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.checkBox_1DB_SVM = new System.Windows.Forms.CheckBox();
            this.checkBox_1DB_C45 = new System.Windows.Forms.CheckBox();
            this.checkBox_1DB_RF = new System.Windows.Forms.CheckBox();
            this.button_add_pathDB = new System.Windows.Forms.Button();
            this.textBox_pathDB = new System.Windows.Forms.TextBox();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.textBox_Name_Exp_2 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.checkBox_SVM = new System.Windows.Forms.CheckBox();
            this.checkBox_C4_5 = new System.Windows.Forms.CheckBox();
            this.checkBox_RF = new System.Windows.Forms.CheckBox();
            this.button_add_pathDB2_test = new System.Windows.Forms.Button();
            this.textBox_pathDB2_test = new System.Windows.Forms.TextBox();
            this.button_add_pathDB2_train = new System.Windows.Forms.Button();
            this.textBox_pathDB2_train = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_resultOut_2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPage5 = new System.Windows.Forms.TabPage();
            this.label17 = new System.Windows.Forms.Label();
            this.comboBox_typeOfAssociation = new System.Windows.Forms.ComboBox();
            this.label15 = new System.Windows.Forms.Label();
            this.comboBox_create_new_DB = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.comboBox_type_output_result_FL = new System.Windows.Forms.ComboBox();
            this.button_add_pathDB_FL = new System.Windows.Forms.Button();
            this.textBox_pathDB_FL = new System.Windows.Forms.TextBox();
            this.textBox_resultOut_FL = new System.Windows.Forms.TextBox();
            this.button_start_FL = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.Out_H_5 = new System.Windows.Forms.TextBox();
            this.Out_M_5 = new System.Windows.Forms.TextBox();
            this.Out_L_5 = new System.Windows.Forms.TextBox();
            this.Out_VL_5 = new System.Windows.Forms.TextBox();
            this.Out_H_4 = new System.Windows.Forms.TextBox();
            this.Out_M_4 = new System.Windows.Forms.TextBox();
            this.Out_L_4 = new System.Windows.Forms.TextBox();
            this.Out_VL_4 = new System.Windows.Forms.TextBox();
            this.Out_H_3 = new System.Windows.Forms.TextBox();
            this.Out_M_3 = new System.Windows.Forms.TextBox();
            this.Out_L_3 = new System.Windows.Forms.TextBox();
            this.Out_VL_3 = new System.Windows.Forms.TextBox();
            this.Out_H_2 = new System.Windows.Forms.TextBox();
            this.Out_M_2 = new System.Windows.Forms.TextBox();
            this.Out_L_2 = new System.Windows.Forms.TextBox();
            this.Out_VL_2 = new System.Windows.Forms.TextBox();
            this.Out_H_1 = new System.Windows.Forms.TextBox();
            this.label38 = new System.Windows.Forms.Label();
            this.Out_M_1 = new System.Windows.Forms.TextBox();
            this.label39 = new System.Windows.Forms.Label();
            this.Out_L_1 = new System.Windows.Forms.TextBox();
            this.label40 = new System.Windows.Forms.Label();
            this.Out_VL_1 = new System.Windows.Forms.TextBox();
            this.label41 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.Dst_host_srv_serror_rate_H_3 = new System.Windows.Forms.TextBox();
            this.Dst_host_srv_serror_rate_M_3 = new System.Windows.Forms.TextBox();
            this.Dst_host_srv_serror_rate_L_3 = new System.Windows.Forms.TextBox();
            this.Dst_host_srv_serror_rate_VL_3 = new System.Windows.Forms.TextBox();
            this.Dst_host_srv_serror_rate_H_2 = new System.Windows.Forms.TextBox();
            this.Dst_host_srv_serror_rate_M_2 = new System.Windows.Forms.TextBox();
            this.Dst_host_srv_serror_rate_L_2 = new System.Windows.Forms.TextBox();
            this.Dst_host_srv_serror_rate_VL_2 = new System.Windows.Forms.TextBox();
            this.Dst_host_srv_serror_rate_H_1 = new System.Windows.Forms.TextBox();
            this.label34 = new System.Windows.Forms.Label();
            this.Dst_host_srv_serror_rate_M_1 = new System.Windows.Forms.TextBox();
            this.label35 = new System.Windows.Forms.Label();
            this.Dst_host_srv_serror_rate_L_1 = new System.Windows.Forms.TextBox();
            this.label36 = new System.Windows.Forms.Label();
            this.Dst_host_srv_serror_rate_VL_1 = new System.Windows.Forms.TextBox();
            this.label37 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.Dst_host_serror_rate_H_3 = new System.Windows.Forms.TextBox();
            this.Dst_host_serror_rate_M_3 = new System.Windows.Forms.TextBox();
            this.Dst_host_serror_rate_L_3 = new System.Windows.Forms.TextBox();
            this.Dst_host_serror_rate_VL_3 = new System.Windows.Forms.TextBox();
            this.Dst_host_serror_rate_H_2 = new System.Windows.Forms.TextBox();
            this.Dst_host_serror_rate_M_2 = new System.Windows.Forms.TextBox();
            this.Dst_host_serror_rate_L_2 = new System.Windows.Forms.TextBox();
            this.Dst_host_serror_rate_VL_2 = new System.Windows.Forms.TextBox();
            this.Dst_host_serror_rate_H_1 = new System.Windows.Forms.TextBox();
            this.label30 = new System.Windows.Forms.Label();
            this.Dst_host_serror_rate_M_1 = new System.Windows.Forms.TextBox();
            this.label31 = new System.Windows.Forms.Label();
            this.Dst_host_serror_rate_L_1 = new System.Windows.Forms.TextBox();
            this.label32 = new System.Windows.Forms.Label();
            this.Dst_host_serror_rate_VL_1 = new System.Windows.Forms.TextBox();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Serror_rate_H_3 = new System.Windows.Forms.TextBox();
            this.Serror_rate_M_3 = new System.Windows.Forms.TextBox();
            this.Serror_rate_L_3 = new System.Windows.Forms.TextBox();
            this.Serror_rate_VL_3 = new System.Windows.Forms.TextBox();
            this.Serror_rate_H_2 = new System.Windows.Forms.TextBox();
            this.Serror_rate_M_2 = new System.Windows.Forms.TextBox();
            this.Serror_rate_L_2 = new System.Windows.Forms.TextBox();
            this.Serror_rate_VL_2 = new System.Windows.Forms.TextBox();
            this.Serror_rate_H_1 = new System.Windows.Forms.TextBox();
            this.label26 = new System.Windows.Forms.Label();
            this.Serror_rate_M_1 = new System.Windows.Forms.TextBox();
            this.label27 = new System.Windows.Forms.Label();
            this.Serror_rate_L_1 = new System.Windows.Forms.TextBox();
            this.label28 = new System.Windows.Forms.Label();
            this.Serror_rate_VL_1 = new System.Windows.Forms.TextBox();
            this.label29 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.Dst_bytes_H_3 = new System.Windows.Forms.TextBox();
            this.Dst_bytes_M_3 = new System.Windows.Forms.TextBox();
            this.Dst_bytes_L_3 = new System.Windows.Forms.TextBox();
            this.Dst_bytes_VL_3 = new System.Windows.Forms.TextBox();
            this.Dst_bytes_H_2 = new System.Windows.Forms.TextBox();
            this.Dst_bytes_M_2 = new System.Windows.Forms.TextBox();
            this.Dst_bytes_L_2 = new System.Windows.Forms.TextBox();
            this.Dst_bytes_VL_2 = new System.Windows.Forms.TextBox();
            this.Dst_bytes_H_1 = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.Dst_bytes_M_1 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.Dst_bytes_L_1 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.Dst_bytes_VL_1 = new System.Windows.Forms.TextBox();
            this.label25 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.Src_bytes_H_3 = new System.Windows.Forms.TextBox();
            this.Src_bytes_M_3 = new System.Windows.Forms.TextBox();
            this.Src_bytes_L_3 = new System.Windows.Forms.TextBox();
            this.Src_bytes_VL_3 = new System.Windows.Forms.TextBox();
            this.Src_bytes_H_2 = new System.Windows.Forms.TextBox();
            this.Src_bytes_M_2 = new System.Windows.Forms.TextBox();
            this.Src_bytes_L_2 = new System.Windows.Forms.TextBox();
            this.Src_bytes_VL_2 = new System.Windows.Forms.TextBox();
            this.Src_bytes_H_1 = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.Src_bytes_M_1 = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.Src_bytes_L_1 = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.Src_bytes_VL_1 = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.saveFileDialog1 = new System.Windows.Forms.SaveFileDialog();
            this.tabControl1.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage5.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(556, 76);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(114, 23);
            this.button1.TabIndex = 0;
            this.button1.Text = "Выполнить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(16, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(110, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Путь к базе данных:";
            // 
            // textBox_resultOut
            // 
            this.textBox_resultOut.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_resultOut.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBox_resultOut.Location = new System.Drawing.Point(19, 115);
            this.textBox_resultOut.Multiline = true;
            this.textBox_resultOut.Name = "textBox_resultOut";
            this.textBox_resultOut.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_resultOut.Size = new System.Drawing.Size(651, 200);
            this.textBox_resultOut.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(16, 81);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(150, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Размер обучающего набора";
            // 
            // textBox_percentSplit
            // 
            this.textBox_percentSplit.Location = new System.Drawing.Point(175, 77);
            this.textBox_percentSplit.Name = "textBox_percentSplit";
            this.textBox_percentSplit.Size = new System.Drawing.Size(45, 20);
            this.textBox_percentSplit.TabIndex = 7;
            this.textBox_percentSplit.Text = "50";
            this.textBox_percentSplit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_percentSplit_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(226, 80);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(18, 15);
            this.label3.TabIndex = 8;
            this.label3.Text = "%";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage5);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.HotTrack = true;
            this.tabControl1.Location = new System.Drawing.Point(12, 12);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(693, 358);
            this.tabControl1.TabIndex = 9;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.label13);
            this.tabPage3.Controls.Add(this.button5);
            this.tabPage3.Controls.Add(this.button4);
            this.tabPage3.Controls.Add(this.textBox_numbTraining);
            this.tabPage3.Controls.Add(this.label10);
            this.tabPage3.Controls.Add(this.features);
            this.tabPage3.Controls.Add(this.label9);
            this.tabPage3.Controls.Add(this.comboBox_type_classification);
            this.tabPage3.Controls.Add(this.Name_DB_creating);
            this.tabPage3.Controls.Add(this.label8);
            this.tabPage3.Controls.Add(this.label7);
            this.tabPage3.Controls.Add(this.comboBox_type_DB);
            this.tabPage3.Controls.Add(this.button3);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(685, 332);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Создание набора данных";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(315, 79);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(103, 13);
            this.label13.TabIndex = 13;
            this.label13.Text = "из 148517 записей";
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(434, 43);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(113, 23);
            this.button5.TabIndex = 11;
            this.button5.Text = "Выбрать 19";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(554, 43);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(113, 23);
            this.button4.TabIndex = 10;
            this.button4.Text = "Выбрать все";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click_1);
            // 
            // textBox_numbTraining
            // 
            this.textBox_numbTraining.Enabled = false;
            this.textBox_numbTraining.Location = new System.Drawing.Point(252, 76);
            this.textBox_numbTraining.Name = "textBox_numbTraining";
            this.textBox_numbTraining.Size = new System.Drawing.Size(57, 20);
            this.textBox_numbTraining.TabIndex = 9;
            this.textBox_numbTraining.Text = "125973";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(25, 79);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(221, 13);
            this.label10.TabIndex = 8;
            this.label10.Text = "Количество записей в обучающем наборе";
            // 
            // features
            // 
            this.features.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.features.CheckOnClick = true;
            this.features.ColumnWidth = 200;
            this.features.FormattingEnabled = true;
            this.features.Items.AddRange(new object[] {
            "1. duration",
            "2. protocol_type",
            "3. service ",
            "4. flag",
            "5. src_bytes ",
            "6. dst_bytes ",
            "7. land",
            "8. wrong_fragment ",
            "9. urgent ",
            "10. hot ",
            "11. num_failed_logins ",
            "12. logged_in",
            "13. num_compromised ",
            "14. root_shell ",
            "15. su_attempted ",
            "16. num_root ",
            "17. num_file_creations ",
            "18. num_shells ",
            "19. num_access_files ",
            "20. num_outbound_cmds ",
            "21. is_host_login ",
            "22. is_guest_login ",
            "23. count ",
            "24. srv_count ",
            "25. serror_rate ",
            "26. srv_serror_rate ",
            "27. rerror_rate ",
            "28. srv_rerror_rate ",
            "29. same_srv_rate",
            "30. diff_srv_rate ",
            "31. srv_diff_host_rate ",
            "32. dst_host_count ",
            "33. dst_host_srv_count ",
            "34. dst_host_same_srv_rate ",
            "35. dst_host_diff_srv_rate ",
            "36. dst_host_same_src_port_rate ",
            "37. dst_host_srv_diff_host_rate ",
            "38. dst_host_serror_rate ",
            "39. dst_host_srv_serror_rate ",
            "40. dst_host_rerror_rate ",
            "41. dst_host_srv_rerror_rate "});
            this.features.Location = new System.Drawing.Point(28, 104);
            this.features.MultiColumn = true;
            this.features.Name = "features";
            this.features.Size = new System.Drawing.Size(639, 214);
            this.features.TabIndex = 7;
            this.features.ThreeDCheckBoxes = true;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(25, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(109, 13);
            this.label9.TabIndex = 6;
            this.label9.Text = "Тип классификации";
            // 
            // comboBox_type_classification
            // 
            this.comboBox_type_classification.FormattingEnabled = true;
            this.comboBox_type_classification.Items.AddRange(new object[] {
            "Нормальные данные / Аномалия ",
            "Нормальные данные / DOS",
            "Нормальные данные / U2R",
            "Нормальные данные / R2L",
            "Нормальные данные / Probe",
            "Нормальные данные / Все подтипы атак",
            "Нормальные данные / DOS  / U2R  / R2L  / Probe"});
            this.comboBox_type_classification.Location = new System.Drawing.Point(150, 45);
            this.comboBox_type_classification.Name = "comboBox_type_classification";
            this.comboBox_type_classification.Size = new System.Drawing.Size(278, 21);
            this.comboBox_type_classification.TabIndex = 5;
            // 
            // Name_DB_creating
            // 
            this.Name_DB_creating.Location = new System.Drawing.Point(90, 13);
            this.Name_DB_creating.Name = "Name_DB_creating";
            this.Name_DB_creating.Size = new System.Drawing.Size(336, 20);
            this.Name_DB_creating.TabIndex = 4;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(25, 15);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(57, 13);
            this.label8.TabIndex = 3;
            this.label8.Text = "Название";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(432, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(65, 13);
            this.label7.TabIndex = 2;
            this.label7.Text = "Тип набора";
            // 
            // comboBox_type_DB
            // 
            this.comboBox_type_DB.FormattingEnabled = true;
            this.comboBox_type_DB.Items.AddRange(new object[] {
            "Разделённый",
            "Единый"});
            this.comboBox_type_DB.Location = new System.Drawing.Point(497, 11);
            this.comboBox_type_DB.Name = "comboBox_type_DB";
            this.comboBox_type_DB.Size = new System.Drawing.Size(169, 21);
            this.comboBox_type_DB.TabIndex = 1;
            this.comboBox_type_DB.SelectionChangeCommitted += new System.EventHandler(this.comboBox_type_DB_SelectionChangeCommitted);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(435, 74);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(232, 23);
            this.button3.TabIndex = 0;
            this.button3.Text = "Создать";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.radioButton2);
            this.tabPage2.Controls.Add(this.radioButton1);
            this.tabPage2.Controls.Add(this.textBox_Name_Exp_1);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.checkBox_1DB_SVM);
            this.tabPage2.Controls.Add(this.checkBox_1DB_C45);
            this.tabPage2.Controls.Add(this.checkBox_1DB_RF);
            this.tabPage2.Controls.Add(this.button_add_pathDB);
            this.tabPage2.Controls.Add(this.textBox_pathDB);
            this.tabPage2.Controls.Add(this.textBox_resultOut);
            this.tabPage2.Controls.Add(this.label3);
            this.tabPage2.Controls.Add(this.button1);
            this.tabPage2.Controls.Add(this.textBox_percentSplit);
            this.tabPage2.Controls.Add(this.label2);
            this.tabPage2.Controls.Add(this.label1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(685, 332);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "МО - Единый набор";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(409, 79);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(110, 17);
            this.radioButton2.TabIndex = 33;
            this.radioButton2.Text = "Выбор от начала";
            this.radioButton2.UseVisualStyleBackColor = true;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Location = new System.Drawing.Point(270, 79);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(115, 17);
            this.radioButton1.TabIndex = 32;
            this.radioButton1.Text = "Случайный выбор";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // textBox_Name_Exp_1
            // 
            this.textBox_Name_Exp_1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_Name_Exp_1.Location = new System.Drawing.Point(81, 46);
            this.textBox_Name_Exp_1.Name = "textBox_Name_Exp_1";
            this.textBox_Name_Exp_1.Size = new System.Drawing.Size(326, 20);
            this.textBox_Name_Exp_1.TabIndex = 31;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(16, 48);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(57, 13);
            this.label12.TabIndex = 30;
            this.label12.Text = "Название";
            // 
            // checkBox_1DB_SVM
            // 
            this.checkBox_1DB_SVM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_1DB_SVM.AutoSize = true;
            this.checkBox_1DB_SVM.Location = new System.Drawing.Point(621, 48);
            this.checkBox_1DB_SVM.Name = "checkBox_1DB_SVM";
            this.checkBox_1DB_SVM.Size = new System.Drawing.Size(49, 17);
            this.checkBox_1DB_SVM.TabIndex = 29;
            this.checkBox_1DB_SVM.Text = "SVM";
            this.checkBox_1DB_SVM.UseVisualStyleBackColor = true;
            // 
            // checkBox_1DB_C45
            // 
            this.checkBox_1DB_C45.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_1DB_C45.AutoSize = true;
            this.checkBox_1DB_C45.Location = new System.Drawing.Point(549, 48);
            this.checkBox_1DB_C45.Name = "checkBox_1DB_C45";
            this.checkBox_1DB_C45.Size = new System.Drawing.Size(48, 17);
            this.checkBox_1DB_C45.TabIndex = 28;
            this.checkBox_1DB_C45.Text = "C4.5";
            this.checkBox_1DB_C45.UseVisualStyleBackColor = true;
            // 
            // checkBox_1DB_RF
            // 
            this.checkBox_1DB_RF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_1DB_RF.AutoSize = true;
            this.checkBox_1DB_RF.Location = new System.Drawing.Point(421, 49);
            this.checkBox_1DB_RF.Name = "checkBox_1DB_RF";
            this.checkBox_1DB_RF.Size = new System.Drawing.Size(98, 17);
            this.checkBox_1DB_RF.TabIndex = 27;
            this.checkBox_1DB_RF.Text = "Random Forest";
            this.checkBox_1DB_RF.UseVisualStyleBackColor = true;
            // 
            // button_add_pathDB
            // 
            this.button_add_pathDB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_add_pathDB.Location = new System.Drawing.Point(562, 15);
            this.button_add_pathDB.Name = "button_add_pathDB";
            this.button_add_pathDB.Size = new System.Drawing.Size(108, 23);
            this.button_add_pathDB.TabIndex = 10;
            this.button_add_pathDB.Text = "Выбрать";
            this.button_add_pathDB.UseVisualStyleBackColor = true;
            this.button_add_pathDB.Click += new System.EventHandler(this.button_add_pathDB_Click);
            // 
            // textBox_pathDB
            // 
            this.textBox_pathDB.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_pathDB.Location = new System.Drawing.Point(132, 15);
            this.textBox_pathDB.Name = "textBox_pathDB";
            this.textBox_pathDB.ReadOnly = true;
            this.textBox_pathDB.Size = new System.Drawing.Size(409, 20);
            this.textBox_pathDB.TabIndex = 9;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.textBox_Name_Exp_2);
            this.tabPage1.Controls.Add(this.label11);
            this.tabPage1.Controls.Add(this.checkBox_SVM);
            this.tabPage1.Controls.Add(this.checkBox_C4_5);
            this.tabPage1.Controls.Add(this.checkBox_RF);
            this.tabPage1.Controls.Add(this.button_add_pathDB2_test);
            this.tabPage1.Controls.Add(this.textBox_pathDB2_test);
            this.tabPage1.Controls.Add(this.button_add_pathDB2_train);
            this.tabPage1.Controls.Add(this.textBox_pathDB2_train);
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.textBox_resultOut_2);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(685, 332);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "МО - Разделенный набор";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // textBox_Name_Exp_2
            // 
            this.textBox_Name_Exp_2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_Name_Exp_2.Location = new System.Drawing.Point(80, 72);
            this.textBox_Name_Exp_2.Name = "textBox_Name_Exp_2";
            this.textBox_Name_Exp_2.Size = new System.Drawing.Size(187, 20);
            this.textBox_Name_Exp_2.TabIndex = 26;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(15, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(57, 13);
            this.label11.TabIndex = 25;
            this.label11.Text = "Название";
            // 
            // checkBox_SVM
            // 
            this.checkBox_SVM.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_SVM.AutoSize = true;
            this.checkBox_SVM.Location = new System.Drawing.Point(494, 74);
            this.checkBox_SVM.Name = "checkBox_SVM";
            this.checkBox_SVM.Size = new System.Drawing.Size(49, 17);
            this.checkBox_SVM.TabIndex = 24;
            this.checkBox_SVM.Text = "SVM";
            this.checkBox_SVM.UseVisualStyleBackColor = true;
            // 
            // checkBox_C4_5
            // 
            this.checkBox_C4_5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_C4_5.AutoSize = true;
            this.checkBox_C4_5.Location = new System.Drawing.Point(412, 74);
            this.checkBox_C4_5.Name = "checkBox_C4_5";
            this.checkBox_C4_5.Size = new System.Drawing.Size(48, 17);
            this.checkBox_C4_5.TabIndex = 23;
            this.checkBox_C4_5.Text = "C4.5";
            this.checkBox_C4_5.UseVisualStyleBackColor = true;
            // 
            // checkBox_RF
            // 
            this.checkBox_RF.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBox_RF.AutoSize = true;
            this.checkBox_RF.Location = new System.Drawing.Point(284, 75);
            this.checkBox_RF.Name = "checkBox_RF";
            this.checkBox_RF.Size = new System.Drawing.Size(98, 17);
            this.checkBox_RF.TabIndex = 22;
            this.checkBox_RF.Text = "Random Forest";
            this.checkBox_RF.UseVisualStyleBackColor = true;
            // 
            // button_add_pathDB2_test
            // 
            this.button_add_pathDB2_test.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_add_pathDB2_test.Location = new System.Drawing.Point(561, 41);
            this.button_add_pathDB2_test.Name = "button_add_pathDB2_test";
            this.button_add_pathDB2_test.Size = new System.Drawing.Size(108, 23);
            this.button_add_pathDB2_test.TabIndex = 21;
            this.button_add_pathDB2_test.Text = "Выбрать";
            this.button_add_pathDB2_test.UseVisualStyleBackColor = true;
            this.button_add_pathDB2_test.Click += new System.EventHandler(this.button_add_pathDB2_test_Click);
            // 
            // textBox_pathDB2_test
            // 
            this.textBox_pathDB2_test.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_pathDB2_test.Location = new System.Drawing.Point(183, 42);
            this.textBox_pathDB2_test.Name = "textBox_pathDB2_test";
            this.textBox_pathDB2_test.ReadOnly = true;
            this.textBox_pathDB2_test.Size = new System.Drawing.Size(362, 20);
            this.textBox_pathDB2_test.TabIndex = 20;
            // 
            // button_add_pathDB2_train
            // 
            this.button_add_pathDB2_train.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_add_pathDB2_train.Location = new System.Drawing.Point(561, 15);
            this.button_add_pathDB2_train.Name = "button_add_pathDB2_train";
            this.button_add_pathDB2_train.Size = new System.Drawing.Size(108, 23);
            this.button_add_pathDB2_train.TabIndex = 19;
            this.button_add_pathDB2_train.Text = "Выбрать";
            this.button_add_pathDB2_train.UseVisualStyleBackColor = true;
            this.button_add_pathDB2_train.Click += new System.EventHandler(this.button_add_pathDB2_train_Click);
            // 
            // textBox_pathDB2_train
            // 
            this.textBox_pathDB2_train.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_pathDB2_train.Location = new System.Drawing.Point(183, 16);
            this.textBox_pathDB2_train.Name = "textBox_pathDB2_train";
            this.textBox_pathDB2_train.ReadOnly = true;
            this.textBox_pathDB2_train.Size = new System.Drawing.Size(362, 20);
            this.textBox_pathDB2_train.TabIndex = 18;
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(561, 70);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(108, 23);
            this.button2.TabIndex = 17;
            this.button2.Text = "Выполнить";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click_1);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label4.Location = new System.Drawing.Point(15, 20);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(148, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Путь к обучающему набору:";
            // 
            // textBox_resultOut_2
            // 
            this.textBox_resultOut_2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_resultOut_2.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBox_resultOut_2.Location = new System.Drawing.Point(18, 99);
            this.textBox_resultOut_2.Multiline = true;
            this.textBox_resultOut_2.Name = "textBox_resultOut_2";
            this.textBox_resultOut_2.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_resultOut_2.Size = new System.Drawing.Size(651, 217);
            this.textBox_resultOut_2.TabIndex = 13;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label6.Location = new System.Drawing.Point(15, 45);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(154, 13);
            this.label6.TabIndex = 15;
            this.label6.Text = "Путь к тестирующим набору:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.label5.Location = new System.Drawing.Point(214, 148);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(110, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Путь к базе данных:";
            // 
            // tabPage5
            // 
            this.tabPage5.Controls.Add(this.label17);
            this.tabPage5.Controls.Add(this.comboBox_typeOfAssociation);
            this.tabPage5.Controls.Add(this.label15);
            this.tabPage5.Controls.Add(this.comboBox_create_new_DB);
            this.tabPage5.Controls.Add(this.label14);
            this.tabPage5.Controls.Add(this.comboBox_type_output_result_FL);
            this.tabPage5.Controls.Add(this.button_add_pathDB_FL);
            this.tabPage5.Controls.Add(this.textBox_pathDB_FL);
            this.tabPage5.Controls.Add(this.textBox_resultOut_FL);
            this.tabPage5.Controls.Add(this.button_start_FL);
            this.tabPage5.Controls.Add(this.label16);
            this.tabPage5.Location = new System.Drawing.Point(4, 22);
            this.tabPage5.Name = "tabPage5";
            this.tabPage5.Size = new System.Drawing.Size(685, 332);
            this.tabPage5.TabIndex = 4;
            this.tabPage5.Text = "Нечёткая логика";
            this.tabPage5.UseVisualStyleBackColor = true;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(15, 75);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(96, 13);
            this.label17.TabIndex = 47;
            this.label17.Text = "Тип объединения";
            // 
            // comboBox_typeOfAssociation
            // 
            this.comboBox_typeOfAssociation.FormattingEnabled = true;
            this.comboBox_typeOfAssociation.Items.AddRange(new object[] {
            "Не объединять",
            "mb_normal относится к нормальным даным, а mb_ anomaly к аномалии",
            "mb_normal и mb_ anomaly относятся к аномалии",
            "mb_normal и mb_ anomaly относятся к нормальным данным"});
            this.comboBox_typeOfAssociation.Location = new System.Drawing.Point(117, 72);
            this.comboBox_typeOfAssociation.Name = "comboBox_typeOfAssociation";
            this.comboBox_typeOfAssociation.Size = new System.Drawing.Size(552, 21);
            this.comboBox_typeOfAssociation.TabIndex = 46;
            this.comboBox_typeOfAssociation.SelectedIndexChanged += new System.EventHandler(this.comboBox_typeOfAssociation_SelectedIndexChanged);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(299, 45);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(276, 13);
            this.label15.TabIndex = 45;
            this.label15.Text = "Создать набор данных с полученными результатами";
            // 
            // comboBox_create_new_DB
            // 
            this.comboBox_create_new_DB.FormattingEnabled = true;
            this.comboBox_create_new_DB.Items.AddRange(new object[] {
            "Нет",
            "Да"});
            this.comboBox_create_new_DB.Location = new System.Drawing.Point(581, 42);
            this.comboBox_create_new_DB.Name = "comboBox_create_new_DB";
            this.comboBox_create_new_DB.Size = new System.Drawing.Size(88, 21);
            this.comboBox_create_new_DB.TabIndex = 44;
            this.comboBox_create_new_DB.SelectedIndexChanged += new System.EventHandler(this.comboBox_create_new_DB_SelectedIndexChanged);
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(15, 45);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(181, 13);
            this.label14.TabIndex = 43;
            this.label14.Text = "Тип вывода результатов на экран";
            // 
            // comboBox_type_output_result_FL
            // 
            this.comboBox_type_output_result_FL.FormattingEnabled = true;
            this.comboBox_type_output_result_FL.Items.AddRange(new object[] {
            "Краткий",
            "Средний",
            "Полный"});
            this.comboBox_type_output_result_FL.Location = new System.Drawing.Point(202, 42);
            this.comboBox_type_output_result_FL.Name = "comboBox_type_output_result_FL";
            this.comboBox_type_output_result_FL.Size = new System.Drawing.Size(91, 21);
            this.comboBox_type_output_result_FL.TabIndex = 42;
            // 
            // button_add_pathDB_FL
            // 
            this.button_add_pathDB_FL.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_add_pathDB_FL.Location = new System.Drawing.Point(513, 14);
            this.button_add_pathDB_FL.Name = "button_add_pathDB_FL";
            this.button_add_pathDB_FL.Size = new System.Drawing.Size(75, 22);
            this.button_add_pathDB_FL.TabIndex = 41;
            this.button_add_pathDB_FL.Text = "Выбрать";
            this.button_add_pathDB_FL.UseVisualStyleBackColor = true;
            this.button_add_pathDB_FL.Click += new System.EventHandler(this.button_add_pathDB_FL_Click);
            // 
            // textBox_pathDB_FL
            // 
            this.textBox_pathDB_FL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_pathDB_FL.Location = new System.Drawing.Point(131, 16);
            this.textBox_pathDB_FL.Name = "textBox_pathDB_FL";
            this.textBox_pathDB_FL.ReadOnly = true;
            this.textBox_pathDB_FL.Size = new System.Drawing.Size(376, 20);
            this.textBox_pathDB_FL.TabIndex = 40;
            // 
            // textBox_resultOut_FL
            // 
            this.textBox_resultOut_FL.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_resultOut_FL.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.textBox_resultOut_FL.Location = new System.Drawing.Point(18, 99);
            this.textBox_resultOut_FL.Multiline = true;
            this.textBox_resultOut_FL.Name = "textBox_resultOut_FL";
            this.textBox_resultOut_FL.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.textBox_resultOut_FL.Size = new System.Drawing.Size(651, 213);
            this.textBox_resultOut_FL.TabIndex = 36;
            // 
            // button_start_FL
            // 
            this.button_start_FL.Location = new System.Drawing.Point(594, 14);
            this.button_start_FL.Name = "button_start_FL";
            this.button_start_FL.Size = new System.Drawing.Size(75, 22);
            this.button_start_FL.TabIndex = 34;
            this.button_start_FL.Text = "Выполнить";
            this.button_start_FL.UseVisualStyleBackColor = true;
            this.button_start_FL.Click += new System.EventHandler(this.button_start_FL_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(15, 19);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(110, 13);
            this.label16.TabIndex = 35;
            this.label16.Text = "Путь к базе данных:";
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox6);
            this.tabPage4.Controls.Add(this.groupBox5);
            this.tabPage4.Controls.Add(this.groupBox4);
            this.tabPage4.Controls.Add(this.groupBox3);
            this.tabPage4.Controls.Add(this.groupBox2);
            this.tabPage4.Controls.Add(this.groupBox1);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(685, 332);
            this.tabPage4.TabIndex = 5;
            this.tabPage4.Text = "Настройка нечёткой логики";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.Out_H_5);
            this.groupBox6.Controls.Add(this.Out_M_5);
            this.groupBox6.Controls.Add(this.Out_L_5);
            this.groupBox6.Controls.Add(this.Out_VL_5);
            this.groupBox6.Controls.Add(this.Out_H_4);
            this.groupBox6.Controls.Add(this.Out_M_4);
            this.groupBox6.Controls.Add(this.Out_L_4);
            this.groupBox6.Controls.Add(this.Out_VL_4);
            this.groupBox6.Controls.Add(this.Out_H_3);
            this.groupBox6.Controls.Add(this.Out_M_3);
            this.groupBox6.Controls.Add(this.Out_L_3);
            this.groupBox6.Controls.Add(this.Out_VL_3);
            this.groupBox6.Controls.Add(this.Out_H_2);
            this.groupBox6.Controls.Add(this.Out_M_2);
            this.groupBox6.Controls.Add(this.Out_L_2);
            this.groupBox6.Controls.Add(this.Out_VL_2);
            this.groupBox6.Controls.Add(this.Out_H_1);
            this.groupBox6.Controls.Add(this.label38);
            this.groupBox6.Controls.Add(this.Out_M_1);
            this.groupBox6.Controls.Add(this.label39);
            this.groupBox6.Controls.Add(this.Out_L_1);
            this.groupBox6.Controls.Add(this.label40);
            this.groupBox6.Controls.Add(this.Out_VL_1);
            this.groupBox6.Controls.Add(this.label41);
            this.groupBox6.Location = new System.Drawing.Point(385, 171);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(286, 119);
            this.groupBox6.TabIndex = 8;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Out / 100";
            // 
            // Out_H_5
            // 
            this.Out_H_5.Location = new System.Drawing.Point(235, 93);
            this.Out_H_5.Name = "Out_H_5";
            this.Out_H_5.Size = new System.Drawing.Size(45, 20);
            this.Out_H_5.TabIndex = 23;
            this.Out_H_5.Text = "100000";
            this.Out_H_5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_M_5
            // 
            this.Out_M_5.Location = new System.Drawing.Point(235, 69);
            this.Out_M_5.Name = "Out_M_5";
            this.Out_M_5.Size = new System.Drawing.Size(45, 20);
            this.Out_M_5.TabIndex = 22;
            this.Out_M_5.Text = "0,8";
            this.Out_M_5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_L_5
            // 
            this.Out_L_5.Location = new System.Drawing.Point(235, 43);
            this.Out_L_5.Name = "Out_L_5";
            this.Out_L_5.Size = new System.Drawing.Size(45, 20);
            this.Out_L_5.TabIndex = 21;
            this.Out_L_5.Text = "0,6";
            this.Out_L_5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_VL_5
            // 
            this.Out_VL_5.Location = new System.Drawing.Point(235, 17);
            this.Out_VL_5.Name = "Out_VL_5";
            this.Out_VL_5.Size = new System.Drawing.Size(45, 20);
            this.Out_VL_5.TabIndex = 20;
            this.Out_VL_5.Text = "0,3";
            this.Out_VL_5.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_H_4
            // 
            this.Out_H_4.Location = new System.Drawing.Point(186, 93);
            this.Out_H_4.Name = "Out_H_4";
            this.Out_H_4.Size = new System.Drawing.Size(45, 20);
            this.Out_H_4.TabIndex = 19;
            this.Out_H_4.Text = "1,1";
            this.Out_H_4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_M_4
            // 
            this.Out_M_4.Location = new System.Drawing.Point(186, 69);
            this.Out_M_4.Name = "Out_M_4";
            this.Out_M_4.Size = new System.Drawing.Size(45, 20);
            this.Out_M_4.TabIndex = 18;
            this.Out_M_4.Text = "0,725";
            this.Out_M_4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_L_4
            // 
            this.Out_L_4.Location = new System.Drawing.Point(186, 43);
            this.Out_L_4.Name = "Out_L_4";
            this.Out_L_4.Size = new System.Drawing.Size(45, 20);
            this.Out_L_4.TabIndex = 17;
            this.Out_L_4.Text = "0,5";
            this.Out_L_4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_VL_4
            // 
            this.Out_VL_4.Location = new System.Drawing.Point(186, 17);
            this.Out_VL_4.Name = "Out_VL_4";
            this.Out_VL_4.Size = new System.Drawing.Size(45, 20);
            this.Out_VL_4.TabIndex = 16;
            this.Out_VL_4.Text = "0,15";
            this.Out_VL_4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_H_3
            // 
            this.Out_H_3.Location = new System.Drawing.Point(135, 93);
            this.Out_H_3.Name = "Out_H_3";
            this.Out_H_3.Size = new System.Drawing.Size(45, 20);
            this.Out_H_3.TabIndex = 15;
            this.Out_H_3.Text = "1";
            this.Out_H_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_M_3
            // 
            this.Out_M_3.Location = new System.Drawing.Point(135, 69);
            this.Out_M_3.Name = "Out_M_3";
            this.Out_M_3.Size = new System.Drawing.Size(45, 20);
            this.Out_M_3.TabIndex = 14;
            this.Out_M_3.Text = "0,65";
            this.Out_M_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_L_3
            // 
            this.Out_L_3.Location = new System.Drawing.Point(135, 43);
            this.Out_L_3.Name = "Out_L_3";
            this.Out_L_3.Size = new System.Drawing.Size(45, 20);
            this.Out_L_3.TabIndex = 13;
            this.Out_L_3.Text = "0,4";
            this.Out_L_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_VL_3
            // 
            this.Out_VL_3.Location = new System.Drawing.Point(135, 17);
            this.Out_VL_3.Name = "Out_VL_3";
            this.Out_VL_3.Size = new System.Drawing.Size(45, 20);
            this.Out_VL_3.TabIndex = 12;
            this.Out_VL_3.Text = "0";
            this.Out_VL_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_H_2
            // 
            this.Out_H_2.Location = new System.Drawing.Point(84, 93);
            this.Out_H_2.Name = "Out_H_2";
            this.Out_H_2.Size = new System.Drawing.Size(45, 20);
            this.Out_H_2.TabIndex = 11;
            this.Out_H_2.Text = "0,9";
            this.Out_H_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_M_2
            // 
            this.Out_M_2.Location = new System.Drawing.Point(84, 69);
            this.Out_M_2.Name = "Out_M_2";
            this.Out_M_2.Size = new System.Drawing.Size(45, 20);
            this.Out_M_2.TabIndex = 10;
            this.Out_M_2.Text = "0,575";
            this.Out_M_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_L_2
            // 
            this.Out_L_2.Location = new System.Drawing.Point(84, 43);
            this.Out_L_2.Name = "Out_L_2";
            this.Out_L_2.Size = new System.Drawing.Size(45, 20);
            this.Out_L_2.TabIndex = 9;
            this.Out_L_2.Text = "0,3";
            this.Out_L_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_VL_2
            // 
            this.Out_VL_2.Location = new System.Drawing.Point(84, 17);
            this.Out_VL_2.Name = "Out_VL_2";
            this.Out_VL_2.Size = new System.Drawing.Size(45, 20);
            this.Out_VL_2.TabIndex = 8;
            this.Out_VL_2.Text = "-0,85";
            this.Out_VL_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Out_H_1
            // 
            this.Out_H_1.Location = new System.Drawing.Point(33, 93);
            this.Out_H_1.Name = "Out_H_1";
            this.Out_H_1.Size = new System.Drawing.Size(45, 20);
            this.Out_H_1.TabIndex = 7;
            this.Out_H_1.Text = "0,7";
            this.Out_H_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Location = new System.Drawing.Point(7, 96);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(15, 13);
            this.label38.TabIndex = 6;
            this.label38.Text = "H";
            // 
            // Out_M_1
            // 
            this.Out_M_1.Location = new System.Drawing.Point(33, 69);
            this.Out_M_1.Name = "Out_M_1";
            this.Out_M_1.Size = new System.Drawing.Size(45, 20);
            this.Out_M_1.TabIndex = 5;
            this.Out_M_1.Text = "0,5";
            this.Out_M_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Location = new System.Drawing.Point(7, 72);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(16, 13);
            this.label39.TabIndex = 4;
            this.label39.Text = "M";
            // 
            // Out_L_1
            // 
            this.Out_L_1.Location = new System.Drawing.Point(33, 43);
            this.Out_L_1.Name = "Out_L_1";
            this.Out_L_1.Size = new System.Drawing.Size(45, 20);
            this.Out_L_1.TabIndex = 3;
            this.Out_L_1.Text = "0,2";
            this.Out_L_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Location = new System.Drawing.Point(7, 46);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(13, 13);
            this.label40.TabIndex = 2;
            this.label40.Text = "L";
            // 
            // Out_VL_1
            // 
            this.Out_VL_1.Location = new System.Drawing.Point(33, 17);
            this.Out_VL_1.Name = "Out_VL_1";
            this.Out_VL_1.Size = new System.Drawing.Size(45, 20);
            this.Out_VL_1.TabIndex = 1;
            this.Out_VL_1.Text = "-1";
            this.Out_VL_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Location = new System.Drawing.Point(7, 20);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(20, 13);
            this.label41.TabIndex = 0;
            this.label41.Text = "VL";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.Dst_host_srv_serror_rate_H_3);
            this.groupBox5.Controls.Add(this.Dst_host_srv_serror_rate_M_3);
            this.groupBox5.Controls.Add(this.Dst_host_srv_serror_rate_L_3);
            this.groupBox5.Controls.Add(this.Dst_host_srv_serror_rate_VL_3);
            this.groupBox5.Controls.Add(this.Dst_host_srv_serror_rate_H_2);
            this.groupBox5.Controls.Add(this.Dst_host_srv_serror_rate_M_2);
            this.groupBox5.Controls.Add(this.Dst_host_srv_serror_rate_L_2);
            this.groupBox5.Controls.Add(this.Dst_host_srv_serror_rate_VL_2);
            this.groupBox5.Controls.Add(this.Dst_host_srv_serror_rate_H_1);
            this.groupBox5.Controls.Add(this.label34);
            this.groupBox5.Controls.Add(this.Dst_host_srv_serror_rate_M_1);
            this.groupBox5.Controls.Add(this.label35);
            this.groupBox5.Controls.Add(this.Dst_host_srv_serror_rate_L_1);
            this.groupBox5.Controls.Add(this.label36);
            this.groupBox5.Controls.Add(this.Dst_host_srv_serror_rate_VL_1);
            this.groupBox5.Controls.Add(this.label37);
            this.groupBox5.Location = new System.Drawing.Point(194, 171);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(178, 119);
            this.groupBox5.TabIndex = 8;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Dst_host_srv_serror_rate / 100";
            // 
            // Dst_host_srv_serror_rate_H_3
            // 
            this.Dst_host_srv_serror_rate_H_3.Location = new System.Drawing.Point(127, 93);
            this.Dst_host_srv_serror_rate_H_3.Name = "Dst_host_srv_serror_rate_H_3";
            this.Dst_host_srv_serror_rate_H_3.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_srv_serror_rate_H_3.TabIndex = 15;
            this.Dst_host_srv_serror_rate_H_3.Text = "2";
            this.Dst_host_srv_serror_rate_H_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_srv_serror_rate_M_3
            // 
            this.Dst_host_srv_serror_rate_M_3.Location = new System.Drawing.Point(127, 69);
            this.Dst_host_srv_serror_rate_M_3.Name = "Dst_host_srv_serror_rate_M_3";
            this.Dst_host_srv_serror_rate_M_3.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_srv_serror_rate_M_3.TabIndex = 14;
            this.Dst_host_srv_serror_rate_M_3.Text = "0,8";
            this.Dst_host_srv_serror_rate_M_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_srv_serror_rate_L_3
            // 
            this.Dst_host_srv_serror_rate_L_3.Location = new System.Drawing.Point(127, 43);
            this.Dst_host_srv_serror_rate_L_3.Name = "Dst_host_srv_serror_rate_L_3";
            this.Dst_host_srv_serror_rate_L_3.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_srv_serror_rate_L_3.TabIndex = 13;
            this.Dst_host_srv_serror_rate_L_3.Text = "0,6";
            this.Dst_host_srv_serror_rate_L_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_srv_serror_rate_VL_3
            // 
            this.Dst_host_srv_serror_rate_VL_3.Location = new System.Drawing.Point(127, 17);
            this.Dst_host_srv_serror_rate_VL_3.Name = "Dst_host_srv_serror_rate_VL_3";
            this.Dst_host_srv_serror_rate_VL_3.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_srv_serror_rate_VL_3.TabIndex = 12;
            this.Dst_host_srv_serror_rate_VL_3.Text = "0,3";
            this.Dst_host_srv_serror_rate_VL_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_srv_serror_rate_H_2
            // 
            this.Dst_host_srv_serror_rate_H_2.Location = new System.Drawing.Point(80, 93);
            this.Dst_host_srv_serror_rate_H_2.Name = "Dst_host_srv_serror_rate_H_2";
            this.Dst_host_srv_serror_rate_H_2.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_srv_serror_rate_H_2.TabIndex = 11;
            this.Dst_host_srv_serror_rate_H_2.Text = "1";
            this.Dst_host_srv_serror_rate_H_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_srv_serror_rate_M_2
            // 
            this.Dst_host_srv_serror_rate_M_2.Location = new System.Drawing.Point(80, 69);
            this.Dst_host_srv_serror_rate_M_2.Name = "Dst_host_srv_serror_rate_M_2";
            this.Dst_host_srv_serror_rate_M_2.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_srv_serror_rate_M_2.TabIndex = 10;
            this.Dst_host_srv_serror_rate_M_2.Text = "0,65";
            this.Dst_host_srv_serror_rate_M_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_srv_serror_rate_L_2
            // 
            this.Dst_host_srv_serror_rate_L_2.Location = new System.Drawing.Point(80, 43);
            this.Dst_host_srv_serror_rate_L_2.Name = "Dst_host_srv_serror_rate_L_2";
            this.Dst_host_srv_serror_rate_L_2.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_srv_serror_rate_L_2.TabIndex = 9;
            this.Dst_host_srv_serror_rate_L_2.Text = "0,4";
            this.Dst_host_srv_serror_rate_L_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_srv_serror_rate_VL_2
            // 
            this.Dst_host_srv_serror_rate_VL_2.Location = new System.Drawing.Point(80, 17);
            this.Dst_host_srv_serror_rate_VL_2.Name = "Dst_host_srv_serror_rate_VL_2";
            this.Dst_host_srv_serror_rate_VL_2.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_srv_serror_rate_VL_2.TabIndex = 8;
            this.Dst_host_srv_serror_rate_VL_2.Text = "0,2";
            this.Dst_host_srv_serror_rate_VL_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_srv_serror_rate_H_1
            // 
            this.Dst_host_srv_serror_rate_H_1.Location = new System.Drawing.Point(33, 93);
            this.Dst_host_srv_serror_rate_H_1.Name = "Dst_host_srv_serror_rate_H_1";
            this.Dst_host_srv_serror_rate_H_1.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_srv_serror_rate_H_1.TabIndex = 7;
            this.Dst_host_srv_serror_rate_H_1.Text = "0,7";
            this.Dst_host_srv_serror_rate_H_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Location = new System.Drawing.Point(7, 96);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(15, 13);
            this.label34.TabIndex = 6;
            this.label34.Text = "H";
            // 
            // Dst_host_srv_serror_rate_M_1
            // 
            this.Dst_host_srv_serror_rate_M_1.Location = new System.Drawing.Point(33, 69);
            this.Dst_host_srv_serror_rate_M_1.Name = "Dst_host_srv_serror_rate_M_1";
            this.Dst_host_srv_serror_rate_M_1.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_srv_serror_rate_M_1.TabIndex = 5;
            this.Dst_host_srv_serror_rate_M_1.Text = "0,5";
            this.Dst_host_srv_serror_rate_M_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Location = new System.Drawing.Point(7, 72);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(16, 13);
            this.label35.TabIndex = 4;
            this.label35.Text = "M";
            // 
            // Dst_host_srv_serror_rate_L_1
            // 
            this.Dst_host_srv_serror_rate_L_1.Location = new System.Drawing.Point(33, 43);
            this.Dst_host_srv_serror_rate_L_1.Name = "Dst_host_srv_serror_rate_L_1";
            this.Dst_host_srv_serror_rate_L_1.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_srv_serror_rate_L_1.TabIndex = 3;
            this.Dst_host_srv_serror_rate_L_1.Text = "0,2";
            this.Dst_host_srv_serror_rate_L_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Location = new System.Drawing.Point(7, 46);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(13, 13);
            this.label36.TabIndex = 2;
            this.label36.Text = "L";
            // 
            // Dst_host_srv_serror_rate_VL_1
            // 
            this.Dst_host_srv_serror_rate_VL_1.Location = new System.Drawing.Point(33, 17);
            this.Dst_host_srv_serror_rate_VL_1.Name = "Dst_host_srv_serror_rate_VL_1";
            this.Dst_host_srv_serror_rate_VL_1.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_srv_serror_rate_VL_1.TabIndex = 1;
            this.Dst_host_srv_serror_rate_VL_1.Text = "-0,1";
            this.Dst_host_srv_serror_rate_VL_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Location = new System.Drawing.Point(7, 20);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(20, 13);
            this.label37.TabIndex = 0;
            this.label37.Text = "VL";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.Dst_host_serror_rate_H_3);
            this.groupBox4.Controls.Add(this.Dst_host_serror_rate_M_3);
            this.groupBox4.Controls.Add(this.Dst_host_serror_rate_L_3);
            this.groupBox4.Controls.Add(this.Dst_host_serror_rate_VL_3);
            this.groupBox4.Controls.Add(this.Dst_host_serror_rate_H_2);
            this.groupBox4.Controls.Add(this.Dst_host_serror_rate_M_2);
            this.groupBox4.Controls.Add(this.Dst_host_serror_rate_L_2);
            this.groupBox4.Controls.Add(this.Dst_host_serror_rate_VL_2);
            this.groupBox4.Controls.Add(this.Dst_host_serror_rate_H_1);
            this.groupBox4.Controls.Add(this.label30);
            this.groupBox4.Controls.Add(this.Dst_host_serror_rate_M_1);
            this.groupBox4.Controls.Add(this.label31);
            this.groupBox4.Controls.Add(this.Dst_host_serror_rate_L_1);
            this.groupBox4.Controls.Add(this.label32);
            this.groupBox4.Controls.Add(this.Dst_host_serror_rate_VL_1);
            this.groupBox4.Controls.Add(this.label33);
            this.groupBox4.Location = new System.Drawing.Point(16, 171);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(172, 119);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Dst_host_serror_rate / 100";
            // 
            // Dst_host_serror_rate_H_3
            // 
            this.Dst_host_serror_rate_H_3.Location = new System.Drawing.Point(125, 93);
            this.Dst_host_serror_rate_H_3.Name = "Dst_host_serror_rate_H_3";
            this.Dst_host_serror_rate_H_3.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_serror_rate_H_3.TabIndex = 15;
            this.Dst_host_serror_rate_H_3.Text = "2";
            this.Dst_host_serror_rate_H_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_serror_rate_M_3
            // 
            this.Dst_host_serror_rate_M_3.Location = new System.Drawing.Point(125, 69);
            this.Dst_host_serror_rate_M_3.Name = "Dst_host_serror_rate_M_3";
            this.Dst_host_serror_rate_M_3.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_serror_rate_M_3.TabIndex = 14;
            this.Dst_host_serror_rate_M_3.Text = "0,8";
            this.Dst_host_serror_rate_M_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_serror_rate_L_3
            // 
            this.Dst_host_serror_rate_L_3.Location = new System.Drawing.Point(125, 43);
            this.Dst_host_serror_rate_L_3.Name = "Dst_host_serror_rate_L_3";
            this.Dst_host_serror_rate_L_3.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_serror_rate_L_3.TabIndex = 13;
            this.Dst_host_serror_rate_L_3.Text = "0,6";
            this.Dst_host_serror_rate_L_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_serror_rate_VL_3
            // 
            this.Dst_host_serror_rate_VL_3.Location = new System.Drawing.Point(125, 17);
            this.Dst_host_serror_rate_VL_3.Name = "Dst_host_serror_rate_VL_3";
            this.Dst_host_serror_rate_VL_3.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_serror_rate_VL_3.TabIndex = 12;
            this.Dst_host_serror_rate_VL_3.Text = "0,3";
            this.Dst_host_serror_rate_VL_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_serror_rate_H_2
            // 
            this.Dst_host_serror_rate_H_2.Location = new System.Drawing.Point(80, 93);
            this.Dst_host_serror_rate_H_2.Name = "Dst_host_serror_rate_H_2";
            this.Dst_host_serror_rate_H_2.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_serror_rate_H_2.TabIndex = 11;
            this.Dst_host_serror_rate_H_2.Text = "1";
            this.Dst_host_serror_rate_H_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_serror_rate_M_2
            // 
            this.Dst_host_serror_rate_M_2.Location = new System.Drawing.Point(80, 69);
            this.Dst_host_serror_rate_M_2.Name = "Dst_host_serror_rate_M_2";
            this.Dst_host_serror_rate_M_2.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_serror_rate_M_2.TabIndex = 10;
            this.Dst_host_serror_rate_M_2.Text = "0,65";
            this.Dst_host_serror_rate_M_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_serror_rate_L_2
            // 
            this.Dst_host_serror_rate_L_2.Location = new System.Drawing.Point(80, 43);
            this.Dst_host_serror_rate_L_2.Name = "Dst_host_serror_rate_L_2";
            this.Dst_host_serror_rate_L_2.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_serror_rate_L_2.TabIndex = 9;
            this.Dst_host_serror_rate_L_2.Text = "0,4";
            this.Dst_host_serror_rate_L_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_serror_rate_VL_2
            // 
            this.Dst_host_serror_rate_VL_2.Location = new System.Drawing.Point(80, 17);
            this.Dst_host_serror_rate_VL_2.Name = "Dst_host_serror_rate_VL_2";
            this.Dst_host_serror_rate_VL_2.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_serror_rate_VL_2.TabIndex = 8;
            this.Dst_host_serror_rate_VL_2.Text = "0,2";
            this.Dst_host_serror_rate_VL_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_host_serror_rate_H_1
            // 
            this.Dst_host_serror_rate_H_1.Location = new System.Drawing.Point(33, 93);
            this.Dst_host_serror_rate_H_1.Name = "Dst_host_serror_rate_H_1";
            this.Dst_host_serror_rate_H_1.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_serror_rate_H_1.TabIndex = 7;
            this.Dst_host_serror_rate_H_1.Text = "0,7";
            this.Dst_host_serror_rate_H_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(7, 96);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(15, 13);
            this.label30.TabIndex = 6;
            this.label30.Text = "H";
            // 
            // Dst_host_serror_rate_M_1
            // 
            this.Dst_host_serror_rate_M_1.Location = new System.Drawing.Point(33, 69);
            this.Dst_host_serror_rate_M_1.Name = "Dst_host_serror_rate_M_1";
            this.Dst_host_serror_rate_M_1.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_serror_rate_M_1.TabIndex = 5;
            this.Dst_host_serror_rate_M_1.Text = "0,5";
            this.Dst_host_serror_rate_M_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Location = new System.Drawing.Point(7, 72);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(16, 13);
            this.label31.TabIndex = 4;
            this.label31.Text = "M";
            // 
            // Dst_host_serror_rate_L_1
            // 
            this.Dst_host_serror_rate_L_1.Location = new System.Drawing.Point(33, 43);
            this.Dst_host_serror_rate_L_1.Name = "Dst_host_serror_rate_L_1";
            this.Dst_host_serror_rate_L_1.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_serror_rate_L_1.TabIndex = 3;
            this.Dst_host_serror_rate_L_1.Text = "0,2";
            this.Dst_host_serror_rate_L_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Location = new System.Drawing.Point(7, 46);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(13, 13);
            this.label32.TabIndex = 2;
            this.label32.Text = "L";
            // 
            // Dst_host_serror_rate_VL_1
            // 
            this.Dst_host_serror_rate_VL_1.Location = new System.Drawing.Point(33, 17);
            this.Dst_host_serror_rate_VL_1.Name = "Dst_host_serror_rate_VL_1";
            this.Dst_host_serror_rate_VL_1.Size = new System.Drawing.Size(41, 20);
            this.Dst_host_serror_rate_VL_1.TabIndex = 1;
            this.Dst_host_serror_rate_VL_1.Text = "-0,1";
            this.Dst_host_serror_rate_VL_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Location = new System.Drawing.Point(7, 20);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(20, 13);
            this.label33.TabIndex = 0;
            this.label33.Text = "VL";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.Serror_rate_H_3);
            this.groupBox3.Controls.Add(this.Serror_rate_M_3);
            this.groupBox3.Controls.Add(this.Serror_rate_L_3);
            this.groupBox3.Controls.Add(this.Serror_rate_VL_3);
            this.groupBox3.Controls.Add(this.Serror_rate_H_2);
            this.groupBox3.Controls.Add(this.Serror_rate_M_2);
            this.groupBox3.Controls.Add(this.Serror_rate_L_2);
            this.groupBox3.Controls.Add(this.Serror_rate_VL_2);
            this.groupBox3.Controls.Add(this.Serror_rate_H_1);
            this.groupBox3.Controls.Add(this.label26);
            this.groupBox3.Controls.Add(this.Serror_rate_M_1);
            this.groupBox3.Controls.Add(this.label27);
            this.groupBox3.Controls.Add(this.Serror_rate_L_1);
            this.groupBox3.Controls.Add(this.label28);
            this.groupBox3.Controls.Add(this.Serror_rate_VL_1);
            this.groupBox3.Controls.Add(this.label29);
            this.groupBox3.Location = new System.Drawing.Point(487, 13);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(184, 119);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Serror_rate  / 100";
            // 
            // Serror_rate_H_3
            // 
            this.Serror_rate_H_3.Location = new System.Drawing.Point(127, 93);
            this.Serror_rate_H_3.Name = "Serror_rate_H_3";
            this.Serror_rate_H_3.Size = new System.Drawing.Size(41, 20);
            this.Serror_rate_H_3.TabIndex = 15;
            this.Serror_rate_H_3.Text = "2";
            this.Serror_rate_H_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Serror_rate_M_3
            // 
            this.Serror_rate_M_3.Location = new System.Drawing.Point(127, 69);
            this.Serror_rate_M_3.Name = "Serror_rate_M_3";
            this.Serror_rate_M_3.Size = new System.Drawing.Size(41, 20);
            this.Serror_rate_M_3.TabIndex = 14;
            this.Serror_rate_M_3.Text = "0,8";
            this.Serror_rate_M_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Serror_rate_L_3
            // 
            this.Serror_rate_L_3.Location = new System.Drawing.Point(127, 43);
            this.Serror_rate_L_3.Name = "Serror_rate_L_3";
            this.Serror_rate_L_3.Size = new System.Drawing.Size(41, 20);
            this.Serror_rate_L_3.TabIndex = 13;
            this.Serror_rate_L_3.Text = "0,6";
            this.Serror_rate_L_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Serror_rate_VL_3
            // 
            this.Serror_rate_VL_3.Location = new System.Drawing.Point(127, 17);
            this.Serror_rate_VL_3.Name = "Serror_rate_VL_3";
            this.Serror_rate_VL_3.Size = new System.Drawing.Size(41, 20);
            this.Serror_rate_VL_3.TabIndex = 12;
            this.Serror_rate_VL_3.Text = "0,3";
            this.Serror_rate_VL_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Serror_rate_H_2
            // 
            this.Serror_rate_H_2.Location = new System.Drawing.Point(80, 93);
            this.Serror_rate_H_2.Name = "Serror_rate_H_2";
            this.Serror_rate_H_2.Size = new System.Drawing.Size(41, 20);
            this.Serror_rate_H_2.TabIndex = 11;
            this.Serror_rate_H_2.Text = "1";
            this.Serror_rate_H_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Serror_rate_M_2
            // 
            this.Serror_rate_M_2.Location = new System.Drawing.Point(80, 69);
            this.Serror_rate_M_2.Name = "Serror_rate_M_2";
            this.Serror_rate_M_2.Size = new System.Drawing.Size(41, 20);
            this.Serror_rate_M_2.TabIndex = 10;
            this.Serror_rate_M_2.Text = "0,65";
            this.Serror_rate_M_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Serror_rate_L_2
            // 
            this.Serror_rate_L_2.Location = new System.Drawing.Point(80, 43);
            this.Serror_rate_L_2.Name = "Serror_rate_L_2";
            this.Serror_rate_L_2.Size = new System.Drawing.Size(41, 20);
            this.Serror_rate_L_2.TabIndex = 9;
            this.Serror_rate_L_2.Text = "0,4";
            this.Serror_rate_L_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Serror_rate_VL_2
            // 
            this.Serror_rate_VL_2.Location = new System.Drawing.Point(80, 17);
            this.Serror_rate_VL_2.Name = "Serror_rate_VL_2";
            this.Serror_rate_VL_2.Size = new System.Drawing.Size(41, 20);
            this.Serror_rate_VL_2.TabIndex = 8;
            this.Serror_rate_VL_2.Text = "0";
            this.Serror_rate_VL_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Serror_rate_H_1
            // 
            this.Serror_rate_H_1.Location = new System.Drawing.Point(33, 93);
            this.Serror_rate_H_1.Name = "Serror_rate_H_1";
            this.Serror_rate_H_1.Size = new System.Drawing.Size(41, 20);
            this.Serror_rate_H_1.TabIndex = 7;
            this.Serror_rate_H_1.Text = "0,7";
            this.Serror_rate_H_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(7, 96);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(15, 13);
            this.label26.TabIndex = 6;
            this.label26.Text = "H";
            // 
            // Serror_rate_M_1
            // 
            this.Serror_rate_M_1.Location = new System.Drawing.Point(33, 69);
            this.Serror_rate_M_1.Name = "Serror_rate_M_1";
            this.Serror_rate_M_1.Size = new System.Drawing.Size(41, 20);
            this.Serror_rate_M_1.TabIndex = 5;
            this.Serror_rate_M_1.Text = "0,5";
            this.Serror_rate_M_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(7, 72);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(16, 13);
            this.label27.TabIndex = 4;
            this.label27.Text = "M";
            // 
            // Serror_rate_L_1
            // 
            this.Serror_rate_L_1.Location = new System.Drawing.Point(33, 43);
            this.Serror_rate_L_1.Name = "Serror_rate_L_1";
            this.Serror_rate_L_1.Size = new System.Drawing.Size(41, 20);
            this.Serror_rate_L_1.TabIndex = 3;
            this.Serror_rate_L_1.Text = "0,2";
            this.Serror_rate_L_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(7, 46);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(13, 13);
            this.label28.TabIndex = 2;
            this.label28.Text = "L";
            // 
            // Serror_rate_VL_1
            // 
            this.Serror_rate_VL_1.Location = new System.Drawing.Point(33, 17);
            this.Serror_rate_VL_1.Name = "Serror_rate_VL_1";
            this.Serror_rate_VL_1.Size = new System.Drawing.Size(41, 20);
            this.Serror_rate_VL_1.TabIndex = 1;
            this.Serror_rate_VL_1.Text = "-0,1";
            this.Serror_rate_VL_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(7, 20);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(20, 13);
            this.label29.TabIndex = 0;
            this.label29.Text = "VL";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.Dst_bytes_H_3);
            this.groupBox2.Controls.Add(this.Dst_bytes_M_3);
            this.groupBox2.Controls.Add(this.Dst_bytes_L_3);
            this.groupBox2.Controls.Add(this.Dst_bytes_VL_3);
            this.groupBox2.Controls.Add(this.Dst_bytes_H_2);
            this.groupBox2.Controls.Add(this.Dst_bytes_M_2);
            this.groupBox2.Controls.Add(this.Dst_bytes_L_2);
            this.groupBox2.Controls.Add(this.Dst_bytes_VL_2);
            this.groupBox2.Controls.Add(this.Dst_bytes_H_1);
            this.groupBox2.Controls.Add(this.label22);
            this.groupBox2.Controls.Add(this.Dst_bytes_M_1);
            this.groupBox2.Controls.Add(this.label23);
            this.groupBox2.Controls.Add(this.Dst_bytes_L_1);
            this.groupBox2.Controls.Add(this.label24);
            this.groupBox2.Controls.Add(this.Dst_bytes_VL_1);
            this.groupBox2.Controls.Add(this.label25);
            this.groupBox2.Location = new System.Drawing.Point(251, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(230, 119);
            this.groupBox2.TabIndex = 8;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Dst_bytes  / 100 000";
            // 
            // Dst_bytes_H_3
            // 
            this.Dst_bytes_H_3.Location = new System.Drawing.Point(165, 93);
            this.Dst_bytes_H_3.Name = "Dst_bytes_H_3";
            this.Dst_bytes_H_3.Size = new System.Drawing.Size(60, 20);
            this.Dst_bytes_H_3.TabIndex = 15;
            this.Dst_bytes_H_3.Text = "100000";
            this.Dst_bytes_H_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_bytes_M_3
            // 
            this.Dst_bytes_M_3.Location = new System.Drawing.Point(165, 69);
            this.Dst_bytes_M_3.Name = "Dst_bytes_M_3";
            this.Dst_bytes_M_3.Size = new System.Drawing.Size(60, 20);
            this.Dst_bytes_M_3.TabIndex = 14;
            this.Dst_bytes_M_3.Text = "0,4";
            this.Dst_bytes_M_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_bytes_L_3
            // 
            this.Dst_bytes_L_3.Location = new System.Drawing.Point(165, 43);
            this.Dst_bytes_L_3.Name = "Dst_bytes_L_3";
            this.Dst_bytes_L_3.Size = new System.Drawing.Size(60, 20);
            this.Dst_bytes_L_3.TabIndex = 13;
            this.Dst_bytes_L_3.Text = "0,6";
            this.Dst_bytes_L_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_bytes_VL_3
            // 
            this.Dst_bytes_VL_3.Location = new System.Drawing.Point(165, 17);
            this.Dst_bytes_VL_3.Name = "Dst_bytes_VL_3";
            this.Dst_bytes_VL_3.Size = new System.Drawing.Size(60, 20);
            this.Dst_bytes_VL_3.TabIndex = 12;
            this.Dst_bytes_VL_3.Text = "0,009";
            this.Dst_bytes_VL_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_bytes_H_2
            // 
            this.Dst_bytes_H_2.Location = new System.Drawing.Point(99, 93);
            this.Dst_bytes_H_2.Name = "Dst_bytes_H_2";
            this.Dst_bytes_H_2.Size = new System.Drawing.Size(60, 20);
            this.Dst_bytes_H_2.TabIndex = 11;
            this.Dst_bytes_H_2.Text = "1";
            this.Dst_bytes_H_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_bytes_M_2
            // 
            this.Dst_bytes_M_2.Location = new System.Drawing.Point(99, 69);
            this.Dst_bytes_M_2.Name = "Dst_bytes_M_2";
            this.Dst_bytes_M_2.Size = new System.Drawing.Size(60, 20);
            this.Dst_bytes_M_2.TabIndex = 10;
            this.Dst_bytes_M_2.Text = "0,2";
            this.Dst_bytes_M_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_bytes_L_2
            // 
            this.Dst_bytes_L_2.Location = new System.Drawing.Point(99, 43);
            this.Dst_bytes_L_2.Name = "Dst_bytes_L_2";
            this.Dst_bytes_L_2.Size = new System.Drawing.Size(60, 20);
            this.Dst_bytes_L_2.TabIndex = 9;
            this.Dst_bytes_L_2.Text = "0,035";
            this.Dst_bytes_L_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_bytes_VL_2
            // 
            this.Dst_bytes_VL_2.Location = new System.Drawing.Point(99, 17);
            this.Dst_bytes_VL_2.Name = "Dst_bytes_VL_2";
            this.Dst_bytes_VL_2.Size = new System.Drawing.Size(60, 20);
            this.Dst_bytes_VL_2.TabIndex = 8;
            this.Dst_bytes_VL_2.Text = "0,004";
            this.Dst_bytes_VL_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Dst_bytes_H_1
            // 
            this.Dst_bytes_H_1.Location = new System.Drawing.Point(33, 93);
            this.Dst_bytes_H_1.Name = "Dst_bytes_H_1";
            this.Dst_bytes_H_1.Size = new System.Drawing.Size(60, 20);
            this.Dst_bytes_H_1.TabIndex = 7;
            this.Dst_bytes_H_1.Text = "0,3";
            this.Dst_bytes_H_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(7, 96);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(15, 13);
            this.label22.TabIndex = 6;
            this.label22.Text = "H";
            // 
            // Dst_bytes_M_1
            // 
            this.Dst_bytes_M_1.Location = new System.Drawing.Point(33, 69);
            this.Dst_bytes_M_1.Name = "Dst_bytes_M_1";
            this.Dst_bytes_M_1.Size = new System.Drawing.Size(60, 20);
            this.Dst_bytes_M_1.TabIndex = 5;
            this.Dst_bytes_M_1.Text = "0,05";
            this.Dst_bytes_M_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(7, 72);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(16, 13);
            this.label23.TabIndex = 4;
            this.label23.Text = "M";
            // 
            // Dst_bytes_L_1
            // 
            this.Dst_bytes_L_1.Location = new System.Drawing.Point(33, 43);
            this.Dst_bytes_L_1.Name = "Dst_bytes_L_1";
            this.Dst_bytes_L_1.Size = new System.Drawing.Size(60, 20);
            this.Dst_bytes_L_1.TabIndex = 3;
            this.Dst_bytes_L_1.Text = "0,007";
            this.Dst_bytes_L_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(7, 46);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(13, 13);
            this.label24.TabIndex = 2;
            this.label24.Text = "L";
            // 
            // Dst_bytes_VL_1
            // 
            this.Dst_bytes_VL_1.Location = new System.Drawing.Point(33, 17);
            this.Dst_bytes_VL_1.Name = "Dst_bytes_VL_1";
            this.Dst_bytes_VL_1.Size = new System.Drawing.Size(60, 20);
            this.Dst_bytes_VL_1.TabIndex = 1;
            this.Dst_bytes_VL_1.Text = "-0,1";
            this.Dst_bytes_VL_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(7, 20);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(20, 13);
            this.label25.TabIndex = 0;
            this.label25.Text = "VL";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.Src_bytes_H_3);
            this.groupBox1.Controls.Add(this.Src_bytes_M_3);
            this.groupBox1.Controls.Add(this.Src_bytes_L_3);
            this.groupBox1.Controls.Add(this.Src_bytes_VL_3);
            this.groupBox1.Controls.Add(this.Src_bytes_H_2);
            this.groupBox1.Controls.Add(this.Src_bytes_M_2);
            this.groupBox1.Controls.Add(this.Src_bytes_L_2);
            this.groupBox1.Controls.Add(this.Src_bytes_VL_2);
            this.groupBox1.Controls.Add(this.Src_bytes_H_1);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.Src_bytes_M_1);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Controls.Add(this.Src_bytes_L_1);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.Src_bytes_VL_1);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Location = new System.Drawing.Point(16, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(229, 119);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Src_bytes / 100 000";
            // 
            // Src_bytes_H_3
            // 
            this.Src_bytes_H_3.Location = new System.Drawing.Point(163, 93);
            this.Src_bytes_H_3.Name = "Src_bytes_H_3";
            this.Src_bytes_H_3.Size = new System.Drawing.Size(60, 20);
            this.Src_bytes_H_3.TabIndex = 15;
            this.Src_bytes_H_3.Text = "100000";
            this.Src_bytes_H_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Src_bytes_M_3
            // 
            this.Src_bytes_M_3.Location = new System.Drawing.Point(163, 69);
            this.Src_bytes_M_3.Name = "Src_bytes_M_3";
            this.Src_bytes_M_3.Size = new System.Drawing.Size(60, 20);
            this.Src_bytes_M_3.TabIndex = 14;
            this.Src_bytes_M_3.Text = "0,4";
            this.Src_bytes_M_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Src_bytes_L_3
            // 
            this.Src_bytes_L_3.Location = new System.Drawing.Point(163, 43);
            this.Src_bytes_L_3.Name = "Src_bytes_L_3";
            this.Src_bytes_L_3.Size = new System.Drawing.Size(60, 20);
            this.Src_bytes_L_3.TabIndex = 13;
            this.Src_bytes_L_3.Text = "0,06";
            this.Src_bytes_L_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Src_bytes_VL_3
            // 
            this.Src_bytes_VL_3.Location = new System.Drawing.Point(163, 17);
            this.Src_bytes_VL_3.Name = "Src_bytes_VL_3";
            this.Src_bytes_VL_3.Size = new System.Drawing.Size(60, 20);
            this.Src_bytes_VL_3.TabIndex = 12;
            this.Src_bytes_VL_3.Text = "0,008";
            this.Src_bytes_VL_3.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Src_bytes_H_2
            // 
            this.Src_bytes_H_2.Location = new System.Drawing.Point(99, 93);
            this.Src_bytes_H_2.Name = "Src_bytes_H_2";
            this.Src_bytes_H_2.Size = new System.Drawing.Size(60, 20);
            this.Src_bytes_H_2.TabIndex = 11;
            this.Src_bytes_H_2.Text = "1";
            this.Src_bytes_H_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Src_bytes_M_2
            // 
            this.Src_bytes_M_2.Location = new System.Drawing.Point(99, 69);
            this.Src_bytes_M_2.Name = "Src_bytes_M_2";
            this.Src_bytes_M_2.Size = new System.Drawing.Size(60, 20);
            this.Src_bytes_M_2.TabIndex = 10;
            this.Src_bytes_M_2.Text = "0,2";
            this.Src_bytes_M_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Src_bytes_L_2
            // 
            this.Src_bytes_L_2.Location = new System.Drawing.Point(99, 43);
            this.Src_bytes_L_2.Name = "Src_bytes_L_2";
            this.Src_bytes_L_2.Size = new System.Drawing.Size(60, 20);
            this.Src_bytes_L_2.TabIndex = 9;
            this.Src_bytes_L_2.Text = "0,03";
            this.Src_bytes_L_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Src_bytes_VL_2
            // 
            this.Src_bytes_VL_2.Location = new System.Drawing.Point(99, 17);
            this.Src_bytes_VL_2.Name = "Src_bytes_VL_2";
            this.Src_bytes_VL_2.Size = new System.Drawing.Size(60, 20);
            this.Src_bytes_VL_2.TabIndex = 8;
            this.Src_bytes_VL_2.Text = "0,004";
            this.Src_bytes_VL_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // Src_bytes_H_1
            // 
            this.Src_bytes_H_1.Location = new System.Drawing.Point(33, 93);
            this.Src_bytes_H_1.Name = "Src_bytes_H_1";
            this.Src_bytes_H_1.Size = new System.Drawing.Size(60, 20);
            this.Src_bytes_H_1.TabIndex = 7;
            this.Src_bytes_H_1.Text = "0,3";
            this.Src_bytes_H_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(7, 96);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(15, 13);
            this.label21.TabIndex = 6;
            this.label21.Text = "H";
            // 
            // Src_bytes_M_1
            // 
            this.Src_bytes_M_1.Location = new System.Drawing.Point(33, 69);
            this.Src_bytes_M_1.Name = "Src_bytes_M_1";
            this.Src_bytes_M_1.Size = new System.Drawing.Size(60, 20);
            this.Src_bytes_M_1.TabIndex = 5;
            this.Src_bytes_M_1.Text = "0,05";
            this.Src_bytes_M_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(7, 72);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(16, 13);
            this.label20.TabIndex = 4;
            this.label20.Text = "M";
            // 
            // Src_bytes_L_1
            // 
            this.Src_bytes_L_1.Location = new System.Drawing.Point(33, 43);
            this.Src_bytes_L_1.Name = "Src_bytes_L_1";
            this.Src_bytes_L_1.Size = new System.Drawing.Size(60, 20);
            this.Src_bytes_L_1.TabIndex = 3;
            this.Src_bytes_L_1.Text = "0,006";
            this.Src_bytes_L_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(7, 46);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(13, 13);
            this.label19.TabIndex = 2;
            this.label19.Text = "L";
            // 
            // Src_bytes_VL_1
            // 
            this.Src_bytes_VL_1.Location = new System.Drawing.Point(33, 17);
            this.Src_bytes_VL_1.Name = "Src_bytes_VL_1";
            this.Src_bytes_VL_1.Size = new System.Drawing.Size(60, 20);
            this.Src_bytes_VL_1.TabIndex = 1;
            this.Src_bytes_VL_1.Text = "-0,1";
            this.Src_bytes_VL_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.FL_KeyPress);
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 20);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(20, 13);
            this.label18.TabIndex = 0;
            this.label18.Text = "VL";
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkTurquoise;
            this.ClientSize = new System.Drawing.Size(716, 382);
            this.Controls.Add(this.tabControl1);
            this.Name = "Form1";
            this.Text = "Traffic classifier";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tabControl1.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            this.tabPage5.ResumeLayout(false);
            this.tabPage5.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox_resultOut;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox textBox_percentSplit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        public System.Windows.Forms.TextBox textBox_resultOut_2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button_add_pathDB;
        private System.Windows.Forms.TextBox textBox_pathDB;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button button_add_pathDB2_test;
        private System.Windows.Forms.TextBox textBox_pathDB2_test;
        private System.Windows.Forms.Button button_add_pathDB2_train;
        private System.Windows.Forms.TextBox textBox_pathDB2_train;
        private System.Windows.Forms.CheckBox checkBox_SVM;
        private System.Windows.Forms.CheckBox checkBox_C4_5;
        private System.Windows.Forms.CheckBox checkBox_RF;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.TextBox Name_DB_creating;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox comboBox_type_DB;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox_type_classification;
        private System.Windows.Forms.CheckedListBox features;
        private System.Windows.Forms.TextBox textBox_numbTraining;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox textBox_Name_Exp_2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox_Name_Exp_1;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox checkBox_1DB_SVM;
        private System.Windows.Forms.CheckBox checkBox_1DB_C45;
        private System.Windows.Forms.CheckBox checkBox_1DB_RF;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TabPage tabPage5;
        private System.Windows.Forms.Button button_add_pathDB_FL;
        private System.Windows.Forms.TextBox textBox_pathDB_FL;
        private System.Windows.Forms.TextBox textBox_resultOut_FL;
        private System.Windows.Forms.Button button_start_FL;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox comboBox_type_output_result_FL;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox comboBox_create_new_DB;
        private System.Windows.Forms.SaveFileDialog saveFileDialog1;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.ComboBox comboBox_typeOfAssociation;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox Out_H_1;
        private System.Windows.Forms.Label label38;
        private System.Windows.Forms.TextBox Out_M_1;
        private System.Windows.Forms.Label label39;
        private System.Windows.Forms.TextBox Out_L_1;
        private System.Windows.Forms.Label label40;
        private System.Windows.Forms.TextBox Out_VL_1;
        private System.Windows.Forms.Label label41;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TextBox Dst_host_srv_serror_rate_H_1;
        private System.Windows.Forms.Label label34;
        private System.Windows.Forms.TextBox Dst_host_srv_serror_rate_M_1;
        private System.Windows.Forms.Label label35;
        private System.Windows.Forms.TextBox Dst_host_srv_serror_rate_L_1;
        private System.Windows.Forms.Label label36;
        private System.Windows.Forms.TextBox Dst_host_srv_serror_rate_VL_1;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox Dst_host_serror_rate_H_1;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.TextBox Dst_host_serror_rate_M_1;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.TextBox Dst_host_serror_rate_L_1;
        private System.Windows.Forms.Label label32;
        private System.Windows.Forms.TextBox Dst_host_serror_rate_VL_1;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox Serror_rate_H_1;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox Serror_rate_M_1;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.TextBox Serror_rate_L_1;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.TextBox Serror_rate_VL_1;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox Dst_bytes_H_1;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox Dst_bytes_M_1;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox Dst_bytes_L_1;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TextBox Dst_bytes_VL_1;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox Src_bytes_H_1;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox Src_bytes_M_1;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox Src_bytes_L_1;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox Src_bytes_VL_1;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox Dst_bytes_H_3;
        private System.Windows.Forms.TextBox Dst_bytes_M_3;
        private System.Windows.Forms.TextBox Dst_bytes_L_3;
        private System.Windows.Forms.TextBox Dst_bytes_VL_3;
        private System.Windows.Forms.TextBox Dst_bytes_H_2;
        private System.Windows.Forms.TextBox Dst_bytes_M_2;
        private System.Windows.Forms.TextBox Dst_bytes_L_2;
        private System.Windows.Forms.TextBox Dst_bytes_VL_2;
        private System.Windows.Forms.TextBox Src_bytes_H_3;
        private System.Windows.Forms.TextBox Src_bytes_M_3;
        private System.Windows.Forms.TextBox Src_bytes_L_3;
        private System.Windows.Forms.TextBox Src_bytes_VL_3;
        private System.Windows.Forms.TextBox Src_bytes_H_2;
        private System.Windows.Forms.TextBox Src_bytes_M_2;
        private System.Windows.Forms.TextBox Src_bytes_L_2;
        private System.Windows.Forms.TextBox Src_bytes_VL_2;
        private System.Windows.Forms.TextBox Out_H_5;
        private System.Windows.Forms.TextBox Out_M_5;
        private System.Windows.Forms.TextBox Out_L_5;
        private System.Windows.Forms.TextBox Out_VL_5;
        private System.Windows.Forms.TextBox Out_H_4;
        private System.Windows.Forms.TextBox Out_M_4;
        private System.Windows.Forms.TextBox Out_L_4;
        private System.Windows.Forms.TextBox Out_VL_4;
        private System.Windows.Forms.TextBox Out_H_3;
        private System.Windows.Forms.TextBox Out_M_3;
        private System.Windows.Forms.TextBox Out_L_3;
        private System.Windows.Forms.TextBox Out_VL_3;
        private System.Windows.Forms.TextBox Out_H_2;
        private System.Windows.Forms.TextBox Out_M_2;
        private System.Windows.Forms.TextBox Out_L_2;
        private System.Windows.Forms.TextBox Out_VL_2;
        private System.Windows.Forms.TextBox Dst_host_srv_serror_rate_H_3;
        private System.Windows.Forms.TextBox Dst_host_srv_serror_rate_M_3;
        private System.Windows.Forms.TextBox Dst_host_srv_serror_rate_L_3;
        private System.Windows.Forms.TextBox Dst_host_srv_serror_rate_VL_3;
        private System.Windows.Forms.TextBox Dst_host_srv_serror_rate_H_2;
        private System.Windows.Forms.TextBox Dst_host_srv_serror_rate_M_2;
        private System.Windows.Forms.TextBox Dst_host_srv_serror_rate_L_2;
        private System.Windows.Forms.TextBox Dst_host_srv_serror_rate_VL_2;
        private System.Windows.Forms.TextBox Dst_host_serror_rate_H_3;
        private System.Windows.Forms.TextBox Dst_host_serror_rate_M_3;
        private System.Windows.Forms.TextBox Dst_host_serror_rate_L_3;
        private System.Windows.Forms.TextBox Dst_host_serror_rate_VL_3;
        private System.Windows.Forms.TextBox Dst_host_serror_rate_H_2;
        private System.Windows.Forms.TextBox Dst_host_serror_rate_M_2;
        private System.Windows.Forms.TextBox Dst_host_serror_rate_L_2;
        private System.Windows.Forms.TextBox Dst_host_serror_rate_VL_2;
        private System.Windows.Forms.TextBox Serror_rate_H_3;
        private System.Windows.Forms.TextBox Serror_rate_M_3;
        private System.Windows.Forms.TextBox Serror_rate_L_3;
        private System.Windows.Forms.TextBox Serror_rate_VL_3;
        private System.Windows.Forms.TextBox Serror_rate_H_2;
        private System.Windows.Forms.TextBox Serror_rate_M_2;
        private System.Windows.Forms.TextBox Serror_rate_L_2;
        private System.Windows.Forms.TextBox Serror_rate_VL_2;
    }
}


﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using AI.Fuzzy.Library;

namespace TrafficClassification
{

    class FuzzyLogic
    {
        StreamWriter streamWriter_FL;
        SugenoFuzzySystem _fsSugeno = null;
        double outResultNumb = 0;

        SugenoFuzzySystem CreateSystem()
        {
            //
            // Create empty Sugeno Fuzzy System
            //
            SugenoFuzzySystem fsSugeno = new SugenoFuzzySystem();

            //
            // Create input variables for the system
            //

           
         

        FuzzyVariable fvSrc_bytes = new FuzzyVariable("Src_bytes", -1.0, 100000.0);//100 000
            fvSrc_bytes.Terms.Add(new FuzzyTerm("VL", new TriangularMembershipFunction(FLMASS.Src_bytes[0,0], FLMASS.Src_bytes[0, 1], FLMASS.Src_bytes[0, 2])));
            fvSrc_bytes.Terms.Add(new FuzzyTerm("L", new TriangularMembershipFunction(FLMASS.Src_bytes[1, 0], FLMASS.Src_bytes[1, 1], FLMASS.Src_bytes[1, 2])));
            fvSrc_bytes.Terms.Add(new FuzzyTerm("M", new TriangularMembershipFunction(FLMASS.Src_bytes[2, 0], FLMASS.Src_bytes[2, 1], FLMASS.Src_bytes[2, 2])));
            fvSrc_bytes.Terms.Add(new FuzzyTerm("H", new TriangularMembershipFunction(FLMASS.Src_bytes[3, 0], FLMASS.Src_bytes[3, 1], FLMASS.Src_bytes[3, 2])));
            fsSugeno.Input.Add(fvSrc_bytes);

            FuzzyVariable fvDst_bytes = new FuzzyVariable("Dst_bytes", -1.0, 100000.0);//100 000
            fvDst_bytes.Terms.Add(new FuzzyTerm("VL", new TriangularMembershipFunction(FLMASS.Dst_bytes[0, 0], FLMASS.Dst_bytes[0, 1], FLMASS.Dst_bytes[0, 2])));
            fvDst_bytes.Terms.Add(new FuzzyTerm("L", new TriangularMembershipFunction(FLMASS.Dst_bytes[1, 0], FLMASS.Dst_bytes[1, 1], FLMASS.Dst_bytes[1, 2])));
            fvDst_bytes.Terms.Add(new FuzzyTerm("M", new TriangularMembershipFunction(FLMASS.Dst_bytes[2, 0], FLMASS.Dst_bytes[2, 1], FLMASS.Dst_bytes[2, 2])));
            fvDst_bytes.Terms.Add(new FuzzyTerm("H", new TriangularMembershipFunction(FLMASS.Dst_bytes[3, 0], FLMASS.Dst_bytes[3, 1], FLMASS.Dst_bytes[3, 2])));
            fsSugeno.Input.Add(fvDst_bytes);


           

            FuzzyVariable fvSerror_rate = new FuzzyVariable("Serror_rate", -1.0, 2.0);//100
            fvSerror_rate.Terms.Add(new FuzzyTerm("VL", new TriangularMembershipFunction(FLMASS.Serror_rate[0, 0], FLMASS.Serror_rate[0, 1], FLMASS.Serror_rate[0, 2])));
            fvSerror_rate.Terms.Add(new FuzzyTerm("L", new TriangularMembershipFunction(FLMASS.Serror_rate[1, 0], FLMASS.Serror_rate[1, 1], FLMASS.Serror_rate[1, 2])));
            fvSerror_rate.Terms.Add(new FuzzyTerm("M", new TriangularMembershipFunction(FLMASS.Serror_rate[2, 0], FLMASS.Serror_rate[2, 1], FLMASS.Serror_rate[2, 2])));
            fvSerror_rate.Terms.Add(new FuzzyTerm("H", new TriangularMembershipFunction(FLMASS.Serror_rate[3, 0], FLMASS.Serror_rate[3, 1], FLMASS.Serror_rate[3, 2])));
            fsSugeno.Input.Add(fvSerror_rate);

            FuzzyVariable fvDst_host_serror_rate = new FuzzyVariable("Dst_host_serror_rate", -1.0, 2.0);//100
            fvDst_host_serror_rate.Terms.Add(new FuzzyTerm("VL", new TriangularMembershipFunction(FLMASS.Dst_host_serror_rate[0, 0], FLMASS.Dst_host_serror_rate[0, 1], FLMASS.Dst_host_serror_rate[0, 2])));
            fvDst_host_serror_rate.Terms.Add(new FuzzyTerm("L", new TriangularMembershipFunction(FLMASS.Dst_host_serror_rate[1, 0], FLMASS.Dst_host_serror_rate[1, 1], FLMASS.Dst_host_serror_rate[1, 2])));
            fvDst_host_serror_rate.Terms.Add(new FuzzyTerm("M", new TriangularMembershipFunction(FLMASS.Dst_host_serror_rate[2, 0], FLMASS.Dst_host_serror_rate[2, 1], FLMASS.Dst_host_serror_rate[2, 2])));
            fvDst_host_serror_rate.Terms.Add(new FuzzyTerm("H", new TriangularMembershipFunction(FLMASS.Dst_host_serror_rate[3, 0], FLMASS.Dst_host_serror_rate[3, 1], FLMASS.Dst_host_serror_rate[3, 2])));
            fsSugeno.Input.Add(fvDst_host_serror_rate);

            FuzzyVariable fvDst_host_srv_serror_rate = new FuzzyVariable("Dst_host_srv_serror_rate", -1.0, 2.0);//100
            fvDst_host_srv_serror_rate.Terms.Add(new FuzzyTerm("VL", new TriangularMembershipFunction(FLMASS.Dst_host_srv_serror_rate[0, 0], FLMASS.Dst_host_srv_serror_rate[0, 1], FLMASS.Dst_host_srv_serror_rate[0, 2])));
            fvDst_host_srv_serror_rate.Terms.Add(new FuzzyTerm("L", new TriangularMembershipFunction(FLMASS.Dst_host_srv_serror_rate[1, 0], FLMASS.Dst_host_srv_serror_rate[1, 1], FLMASS.Dst_host_srv_serror_rate[1, 2])));
            fvDst_host_srv_serror_rate.Terms.Add(new FuzzyTerm("M", new TriangularMembershipFunction(FLMASS.Dst_host_srv_serror_rate[2, 0], FLMASS.Dst_host_srv_serror_rate[2, 1], FLMASS.Dst_host_srv_serror_rate[2, 2])));
            fvDst_host_srv_serror_rate.Terms.Add(new FuzzyTerm("H", new TriangularMembershipFunction(FLMASS.Dst_host_srv_serror_rate[3, 0], FLMASS.Dst_host_srv_serror_rate[3, 1], FLMASS.Dst_host_srv_serror_rate[3, 2])));
            fsSugeno.Input.Add(fvDst_host_srv_serror_rate);

           
            //
            // Create output variables for the system
            //
         
             SugenoVariable svAnomaly = new SugenoVariable("Anomaly");
             svAnomaly.Functions.Add(fsSugeno.CreateSugenoFunction("normal", new double[] { FLMASS.Out[0, 0], FLMASS.Out[0, 1], FLMASS.Out[0, 2], FLMASS.Out[0, 3], FLMASS.Out[0, 4] }));
             svAnomaly.Functions.Add(fsSugeno.CreateSugenoFunction("mb_normal", new double[] { FLMASS.Out[1, 0], FLMASS.Out[1, 1], FLMASS.Out[1, 2], FLMASS.Out[1, 3], FLMASS.Out[1, 4] }));
             svAnomaly.Functions.Add(fsSugeno.CreateSugenoFunction("mb_anomaly", new double[] { FLMASS.Out[2, 0], FLMASS.Out[2, 1], FLMASS.Out[2, 2], FLMASS.Out[2, 3], FLMASS.Out[2, 4] }));
             svAnomaly.Functions.Add(fsSugeno.CreateSugenoFunction("anomaly", new double[] { FLMASS.Out[3, 0], FLMASS.Out[3, 1], FLMASS.Out[3, 2], FLMASS.Out[3, 3], FLMASS.Out[3, 4] }));
             fsSugeno.Output.Add(svAnomaly);
             

            //
            // Create fuzzy rules
            //
            try
            {
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "VL", "VL", "anomaly"); //1
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "VL", "L", "normal"); //2
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "VL", "M", "normal"); //3
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "VL", "H", "normal"); //4
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "L", "VL", "normal"); //5
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "L", "L", "normal"); //6
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "L", "M", "normal"); //7
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "L", "H", "normal"); //8
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "M", "VL", "normal"); //9
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "M", "L", "normal"); //10
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "M", "M", "normal"); //11
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "M", "H", "normal"); //12
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "H", "VL", "normal"); //13
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "H", "L", "normal"); //14
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "H", "M", "normal"); //15
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "VL", "H", "H", "normal"); //16
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "VL", "VL", "normal"); //17
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "VL", "L", "normal"); //18
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "VL", "M", "normal"); //19
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "VL", "H", "normal"); //20
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "L", "VL", "normal"); //21
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "L", "L", "mb_normal"); //22
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "L", "M", "normal"); //23
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "L", "H", "normal"); //24
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "M", "VL", "normal"); //25
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "M", "L", "normal"); //26
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "M", "M", "normal"); //27
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "M", "H", "normal"); //28
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "H", "VL", "normal"); //29
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "H", "L", "normal"); //30
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "H", "M", "normal"); //31
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "L", "H", "H", "normal"); //32
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "VL", "VL", "normal"); //33
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "VL", "L", "normal"); //34
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "VL", "M", "normal"); //35
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "VL", "H", "normal"); //36
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "L", "VL", "normal"); //37
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "L", "L", "normal"); //38
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "L", "M", "normal"); //39
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "L", "H", "normal"); //40
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "M", "VL", "normal"); //41
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "M", "L", "normal"); //42
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "M", "M", "mb_anomaly"); //43
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "M", "H", "normal"); //44
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "H", "VL", "normal"); //45
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "H", "L", "normal"); //46
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "H", "M", "normal"); //47
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "M", "H", "H", "normal"); //48
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "VL", "VL", "normal"); //49
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "VL", "L", "normal"); //50
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "VL", "M", "normal"); //51
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "VL", "H", "normal"); //52
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "L", "VL", "normal"); //53
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "L", "L", "normal"); //54
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "L", "M", "normal"); //55
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "L", "H", "normal"); //56
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "M", "VL", "normal"); //57
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "M", "L", "normal"); //58
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "M", "M", "normal"); //59
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "M", "H", "normal"); //60
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "H", "VL", "normal"); //61
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "H", "L", "normal"); //62
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "H", "M", "normal"); //63
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "VL", "H", "H", "H", "anomaly"); //64
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "VL", "VL", "normal"); //65
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "VL", "L", "normal"); //66
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "VL", "M", "normal"); //67
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "VL", "H", "normal"); //68
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "L", "VL", "normal"); //69
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "L", "L", "mb_normal"); //70
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "L", "M", "normal"); //71
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "L", "H", "normal"); //72
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "M", "VL", "normal"); //73
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "M", "L", "normal"); //74
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "M", "M", "normal"); //75
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "M", "H", "normal"); //76
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "H", "VL", "normal"); //77
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "H", "L", "normal"); //78
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "H", "M", "normal"); //79
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "VL", "H", "H", "normal"); //80
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "VL", "VL", "normal"); //81
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "VL", "L", "mb_normal"); //82
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "VL", "M", "normal"); //83
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "VL", "H", "normal"); //84
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "L", "VL", "mb_normal"); //85
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "L", "L", "mb_normal"); //86
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "L", "M", "mb_normal"); //87
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "L", "H", "mb_normal"); //88
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "M", "VL", "normal"); //89
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "M", "L", "mb_normal"); //90
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "M", "M", "mb_normal"); //91
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "M", "H", "mb_normal"); //92
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "H", "VL", "normal"); //93
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "H", "L", "mb_normal"); //94
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "H", "M", "mb_normal"); //95
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "L", "H", "H", "mb_normal"); //96
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "VL", "VL", "normal"); //97
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "VL", "L", "normal"); //98
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "VL", "M", "normal"); //99
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "VL", "H", "normal"); //100
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "L", "VL", "normal"); //101
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "L", "L", "mb_normal"); //102
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "L", "M", "mb_normal"); //103
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "L", "H", "mb_normal"); //104
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "M", "VL", "normal"); //105
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "M", "L", "mb_normal"); //106
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "M", "M", "mb_anomaly"); //107
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "M", "H", "mb_anomaly"); //108
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "H", "VL", "normal"); //109
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "H", "L", "mb_normal"); //110
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "H", "M", "mb_anomaly"); //111
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "M", "H", "H", "anomaly"); //112
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "VL", "VL", "normal"); //113
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "VL", "L", "normal"); //114
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "VL", "M", "normal"); //115
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "VL", "H", "normal"); //116
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "L", "VL", "normal"); //117
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "L", "L", "mb_normal"); //118
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "L", "M", "mb_normal"); //119
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "L", "H", "mb_normal"); //120
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "M", "VL", "normal"); //121
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "M", "L", "mb_normal"); //122
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "M", "M", "mb_anomaly"); //123
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "M", "H", "anomaly"); //124
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "H", "VL", "normal"); //125
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "H", "L", "mb_normal"); //126
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "H", "M", "anomaly"); //127
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "L", "H", "H", "H", "anomaly"); //128
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "VL", "VL", "normal"); //129
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "VL", "L", "normal"); //130
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "VL", "M", "normal"); //131
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "VL", "H", "normal"); //132
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "L", "VL", "normal"); //133
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "L", "L", "normal"); //134
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "L", "M", "normal"); //135
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "L", "H", "normal"); //136
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "M", "VL", "normal"); //137
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "M", "L", "normal"); //138
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "M", "M", "mb_anomaly"); //139
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "M", "H", "normal"); //140
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "H", "VL", "normal"); //141
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "H", "L", "normal"); //142
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "H", "M", "normal"); //143
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "VL", "H", "H", "normal"); //144
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "VL", "VL", "normal"); //145
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "VL", "L", "normal"); //146
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "VL", "M", "normal"); //147
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "VL", "H", "normal"); //148
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "L", "VL", "normal"); //149
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "L", "L", "mb_normal"); //150
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "L", "M", "mb_normal"); //151
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "L", "H", "mb_normal"); //152
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "M", "VL", "normal"); //153
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "M", "L", "mb_normal"); //154
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "M", "M", "mb_anomaly"); //155
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "M", "H", "mb_anomaly"); //156
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "H", "VL", "normal"); //157
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "H", "L", "mb_normal"); //158
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "H", "M", "mb_anomaly"); //159
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "L", "H", "H", "anomaly"); //160
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "VL", "VL", "normal"); //161
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "VL", "L", "normal"); //162
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "VL", "M", "mb_anomaly"); //163
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "VL", "H", "normal"); //164
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "L", "VL", "normal"); //165
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "L", "L", "mb_normal"); //166
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "L", "M", "mb_anomaly"); //167
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "L", "H", "mb_anomaly"); //168
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "M", "VL", "mb_anomaly"); //169
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "M", "L", "mb_anomaly"); //170
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "M", "M", "mb_anomaly"); //171
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "M", "H", "mb_anomaly"); //172
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "H", "VL", "normal"); //173
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "H", "L", "mb_anomaly"); //174
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "H", "M", "mb_anomaly"); //175
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "M", "H", "H", "mb_anomaly"); //176
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "VL", "VL", "normal"); //177
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "VL", "L", "normal"); //178
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "VL", "M", "normal"); //179
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "VL", "H", "normal"); //180
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "L", "VL", "normal"); //181
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "L", "L", "mb_normal"); //182
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "L", "M", "mb_anomaly"); //183
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "L", "H", "anomaly"); //184
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "M", "VL", "normal"); //185
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "M", "L", "mb_anomaly"); //186
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "M", "M", "mb_anomaly"); //187
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "M", "H", "mb_anomaly"); //188
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "H", "VL", "normal"); //189
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "H", "L", "anomaly"); //190
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "H", "M", "mb_anomaly"); //191
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "M", "H", "H", "H", "anomaly"); //192
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "VL", "VL", "normal"); //193
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "VL", "L", "normal"); //194
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "VL", "M", "normal"); //195
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "VL", "H", "normal"); //196
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "L", "VL", "normal"); //197
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "L", "L", "normal"); //198
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "L", "M", "normal"); //199
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "L", "H", "normal"); //200
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "M", "VL", "normal"); //201
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "M", "L", "normal"); //202
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "M", "M", "normal"); //203
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "M", "H", "normal"); //204
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "H", "VL", "normal"); //205
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "H", "L", "normal"); //206
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "H", "M", "normal"); //207
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "VL", "H", "H", "anomaly"); //208
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "VL", "VL", "normal"); //209
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "VL", "L", "normal"); //210
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "VL", "M", "normal"); //211
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "VL", "H", "normal"); //212
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "L", "VL", "normal"); //213
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "L", "L", "mb_normal"); //214
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "L", "M", "mb_normal"); //215
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "L", "H", "mb_normal"); //216
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "M", "VL", "normal"); //217
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "M", "L", "mb_normal"); //218
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "M", "M", "mb_anomaly"); //219
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "M", "H", "anomaly"); //220
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "H", "VL", "normal"); //221
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "H", "L", "mb_normal"); //222
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "H", "M", "anomaly"); //223
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "L", "H", "H", "anomaly"); //224
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "VL", "VL", "normal"); //225
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "VL", "L", "normal"); //226
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "VL", "M", "normal"); //227
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "VL", "H", "normal"); //228
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "L", "VL", "normal"); //229
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "L", "L", "mb_normal"); //230
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "L", "M", "mb_anomaly"); //231
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "L", "H", "anomaly"); //232
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "M", "VL", "normal"); //233
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "M", "L", "mb_anomaly"); //234
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "M", "M", "mb_anomaly"); //235
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "M", "H", "mb_anomaly"); //236
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "H", "VL", "normal"); //237
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "H", "L", "anomaly"); //238
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "H", "M", "mb_anomaly"); //239
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "M", "H", "H", "anomaly"); //240
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "VL", "VL", "normal"); //241
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "VL", "L", "normal"); //242
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "VL", "M", "normal"); //243
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "VL", "H", "anomaly"); //244
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "L", "VL", "normal"); //245
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "L", "L", "mb_normal"); //246
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "L", "M", "anomaly"); //247
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "L", "H", "anomaly"); //248
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "M", "VL", "normal"); //249
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "M", "L", "anomaly"); //250
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "M", "M", "mb_anomaly"); //251
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "M", "H", "anomaly"); //252
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "H", "VL", "anomaly"); //253
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "H", "L", "anomaly"); //254
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "H", "M", "anomaly"); //255
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "VL", "H", "H", "H", "H", "anomaly"); //256
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "VL", "VL", "normal"); //257
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "VL", "L", "normal"); //258
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "VL", "M", "normal"); //259
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "VL", "H", "normal"); //260
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "L", "VL", "normal"); //261
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "L", "L", "mb_normal"); //262
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "L", "M", "normal"); //263
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "L", "H", "normal"); //264
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "M", "VL", "normal"); //265
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "M", "L", "normal"); //266
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "M", "M", "normal"); //267
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "M", "H", "normal"); //268
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "H", "VL", "normal"); //269
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "H", "L", "normal"); //270
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "H", "M", "normal"); //271
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "VL", "H", "H", "normal"); //272
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "VL", "VL", "normal"); //273
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "VL", "L", "mb_normal"); //274
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "VL", "M", "normal"); //275
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "VL", "H", "normal"); //276
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "L", "VL", "mb_normal"); //277
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "L", "L", "mb_normal"); //278
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "L", "M", "mb_normal"); //279
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "L", "H", "mb_normal"); //280
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "M", "VL", "normal"); //281
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "M", "L", "mb_normal"); //282
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "M", "M", "mb_normal"); //283
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "M", "H", "mb_normal"); //284
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "H", "VL", "normal"); //285
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "H", "L", "mb_normal"); //286
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "H", "M", "mb_normal"); //287
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "L", "H", "H", "mb_normal"); //288
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "VL", "VL", "normal"); //289
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "VL", "L", "normal"); //290
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "VL", "M", "normal"); //291
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "VL", "H", "normal"); //292
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "L", "VL", "normal"); //293
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "L", "L", "mb_normal"); //294
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "L", "M", "mb_normal"); //295
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "L", "H", "mb_normal"); //296
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "M", "VL", "normal"); //297
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "M", "L", "mb_normal"); //298
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "M", "M", "mb_anomaly"); //299
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "M", "H", "mb_anomaly"); //300
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "H", "VL", "normal"); //301
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "H", "L", "mb_normal"); //302
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "H", "M", "mb_anomaly"); //303
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "M", "H", "H", "anomaly"); //304
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "VL", "VL", "normal"); //305
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "VL", "L", "normal"); //306
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "VL", "M", "normal"); //307
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "VL", "H", "normal"); //308
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "L", "VL", "normal"); //309
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "L", "L", "mb_normal"); //310
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "L", "M", "mb_normal"); //311
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "L", "H", "mb_normal"); //312
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "M", "VL", "normal"); //313
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "M", "L", "mb_normal"); //314
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "M", "M", "mb_anomaly"); //315
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "M", "H", "anomaly"); //316
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "H", "VL", "normal"); //317
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "H", "L", "mb_normal"); //318
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "H", "M", "anomaly"); //319
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "VL", "H", "H", "H", "anomaly"); //320
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "VL", "VL", "normal"); //321
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "VL", "L", "mb_normal"); //322
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "VL", "M", "normal"); //323
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "VL", "H", "normal"); //324
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "L", "VL", "mb_normal"); //325
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "L", "L", "mb_normal"); //326
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "L", "M", "mb_normal"); //327
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "L", "H", "mb_normal"); //328
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "M", "VL", "normal"); //329
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "M", "L", "mb_normal"); //330
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "M", "M", "mb_normal"); //331
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "M", "H", "mb_normal"); //332
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "H", "VL", "normal"); //333
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "H", "L", "mb_normal"); //334
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "H", "M", "mb_normal"); //335
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "VL", "H", "H", "mb_normal"); //336
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "VL", "VL", "mb_normal"); //337
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "VL", "L", "mb_normal"); //338
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "VL", "M", "mb_normal"); //339
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "VL", "H", "mb_normal"); //340
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "L", "VL", "mb_normal"); //341
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "L", "L", "mb_normal"); //342
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "L", "M", "mb_normal"); //343
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "L", "H", "mb_normal"); //344
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "M", "VL", "mb_normal"); //345
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "M", "L", "mb_normal"); //346
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "M", "M", "mb_normal"); //347
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "M", "H", "mb_normal"); //348
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "H", "VL", "mb_normal"); //349
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "H", "L", "mb_normal"); //350
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "H", "M", "mb_normal"); //351
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "L", "H", "H", "mb_normal"); //352
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "VL", "VL", "normal"); //353
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "VL", "L", "mb_normal"); //354
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "VL", "M", "mb_normal"); //355
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "VL", "H", "mb_normal"); //356
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "L", "VL", "mb_normal"); //357
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "L", "L", "mb_normal"); //358
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "L", "M", "mb_normal"); //359
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "L", "H", "mb_normal"); //360
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "M", "VL", "mb_normal"); //361
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "M", "L", "mb_normal"); //362
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "M", "M", "mb_anomaly"); //363
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "M", "H", "mb_normal"); //364
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "H", "VL", "mb_normal"); //365
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "H", "L", "mb_normal"); //366
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "H", "M", "mb_normal"); //367
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "M", "H", "H", "mb_normal"); //368
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "VL", "VL", "normal"); //369
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "VL", "L", "mb_normal"); //370
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "VL", "M", "mb_normal"); //371
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "VL", "H", "mb_normal"); //372
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "L", "VL", "mb_normal"); //373
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "L", "L", "mb_normal"); //374
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "L", "M", "mb_normal"); //375
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "L", "H", "mb_normal"); //376
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "M", "VL", "mb_normal"); //377
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "M", "L", "mb_normal"); //378
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "M", "M", "mb_normal"); //379
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "M", "H", "mb_normal"); //380
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "H", "VL", "mb_normal"); //381
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "H", "L", "mb_normal"); //382
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "H", "M", "mb_normal"); //383
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "L", "H", "H", "H", "anomaly"); //384
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "VL", "VL", "normal"); //385
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "VL", "L", "normal"); //386
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "VL", "M", "normal"); //387
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "VL", "H", "normal"); //388
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "L", "VL", "normal"); //389
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "L", "L", "mb_normal"); //390
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "L", "M", "mb_normal"); //391
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "L", "H", "mb_normal"); //392
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "M", "VL", "normal"); //393
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "M", "L", "mb_normal"); //394
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "M", "M", "mb_anomaly"); //395
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "M", "H", "mb_anomaly"); //396
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "H", "VL", "normal"); //397
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "H", "L", "mb_normal"); //398
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "H", "M", "mb_anomaly"); //399
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "VL", "H", "H", "anomaly"); //400
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "VL", "VL", "normal"); //401
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "VL", "L", "mb_normal"); //402
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "VL", "M", "mb_normal"); //403
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "VL", "H", "mb_normal"); //404
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "L", "VL", "mb_normal"); //405
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "L", "L", "mb_normal"); //406
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "L", "M", "mb_normal"); //407
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "L", "H", "mb_normal"); //408
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "M", "VL", "mb_normal"); //409
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "M", "L", "mb_normal"); //410
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "M", "M", "mb_anomaly"); //411
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "M", "H", "mb_normal"); //412
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "H", "VL", "mb_normal"); //413
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "H", "L", "mb_normal"); //414
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "H", "M", "mb_normal"); //415
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "L", "H", "H", "mb_normal"); //416
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "VL", "VL", "normal"); //417
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "VL", "L", "mb_normal"); //418
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "VL", "M", "mb_anomaly"); //419
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "VL", "H", "mb_anomaly"); //420
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "L", "VL", "mb_normal"); //421
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "L", "L", "mb_normal"); //422
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "L", "M", "mb_anomaly"); //423
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "L", "H", "mb_normal"); //424
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "M", "VL", "mb_anomaly"); //425
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "M", "L", "mb_anomaly"); //426
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "M", "M", "mb_anomaly"); //427
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "M", "H", "mb_anomaly"); //428
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "H", "VL", "mb_anomaly"); //429
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "H", "L", "mb_normal"); //430
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "H", "M", "mb_anomaly"); //431
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "M", "H", "H", "mb_anomaly"); //432
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "VL", "VL", "normal"); //433
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "VL", "L", "mb_normal"); //434
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "VL", "M", "mb_anomaly"); //435
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "VL", "H", "anomaly"); //436
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "L", "VL", "mb_normal"); //437
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "L", "L", "mb_normal"); //438
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "L", "M", "mb_normal"); //439
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "L", "H", "mb_normal"); //440
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "M", "VL", "mb_anomaly"); //441
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "M", "L", "mb_normal"); //442
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "M", "M", "mb_anomaly"); //443
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "M", "H", "mb_anomaly"); //444
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "H", "VL", "anomaly"); //445
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "H", "L", "mb_normal"); //446
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "H", "M", "mb_anomaly"); //447
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "M", "H", "H", "H", "anomaly"); //448
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "VL", "VL", "normal"); //449
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "VL", "L", "normal"); //450
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "VL", "M", "normal"); //451
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "VL", "H", "normal"); //452
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "L", "VL", "normal"); //453
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "L", "L", "mb_normal"); //454
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "L", "M", "mb_normal"); //455
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "L", "H", "mb_normal"); //456
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "M", "VL", "normal"); //457
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "M", "L", "mb_normal"); //458
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "M", "M", "mb_anomaly"); //459
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "M", "H", "anomaly"); //460
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "H", "VL", "normal"); //461
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "H", "L", "mb_normal"); //462
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "H", "M", "anomaly"); //463
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "VL", "H", "H", "anomaly"); //464
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "VL", "VL", "normal"); //465
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "VL", "L", "mb_normal"); //466
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "VL", "M", "mb_normal"); //467
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "VL", "H", "mb_normal"); //468
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "L", "VL", "mb_normal"); //469
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "L", "L", "mb_normal"); //470
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "L", "M", "mb_normal"); //471
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "L", "H", "mb_normal"); //472
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "M", "VL", "mb_normal"); //473
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "M", "L", "mb_normal"); //474
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "M", "M", "mb_normal"); //475
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "M", "H", "mb_normal"); //476
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "H", "VL", "mb_normal"); //477
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "H", "L", "mb_normal"); //478
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "H", "M", "mb_normal"); //479
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "L", "H", "H", "anomaly"); //480
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "VL", "VL", "normal"); //481
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "VL", "L", "mb_normal"); //482
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "VL", "M", "mb_anomaly"); //483
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "VL", "H", "anomaly"); //484
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "L", "VL", "mb_normal"); //485
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "L", "L", "mb_normal"); //486
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "L", "M", "mb_normal"); //487
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "L", "H", "mb_normal"); //488
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "M", "VL", "mb_anomaly"); //489
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "M", "L", "mb_normal"); //490
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "M", "M", "mb_anomaly"); //491
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "M", "H", "mb_anomaly"); //492
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "H", "VL", "anomaly"); //493
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "H", "L", "mb_normal"); //494
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "H", "M", "mb_anomaly"); //495
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "M", "H", "H", "anomaly"); //496
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "VL", "VL", "normal"); //497
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "VL", "L", "mb_normal"); //498
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "VL", "M", "anomaly"); //499
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "VL", "H", "anomaly"); //500
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "L", "VL", "mb_normal"); //501
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "L", "L", "mb_normal"); //502
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "L", "M", "mb_normal"); //503
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "L", "H", "anomaly"); //504
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "M", "VL", "anomaly"); //505
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "M", "L", "mb_normal"); //506
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "M", "M", "mb_anomaly"); //507
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "M", "H", "anomaly"); //508
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "H", "VL", "anomaly"); //509
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "H", "L", "anomaly"); //510
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "H", "M", "anomaly"); //511
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "L", "H", "H", "H", "H", "anomaly"); //512
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "VL", "VL", "normal"); //513
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "VL", "L", "normal"); //514
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "VL", "M", "normal"); //515
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "VL", "H", "normal"); //516
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "L", "VL", "normal"); //517
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "L", "L", "normal"); //518
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "L", "M", "normal"); //519
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "L", "H", "normal"); //520
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "M", "VL", "normal"); //521
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "M", "L", "normal"); //522
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "M", "M", "mb_anomaly"); //523
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "M", "H", "normal"); //524
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "H", "VL", "normal"); //525
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "H", "L", "normal"); //526
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "H", "M", "normal"); //527
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "VL", "H", "H", "normal"); //528
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "VL", "VL", "normal"); //529
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "VL", "L", "normal"); //530
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "VL", "M", "normal"); //531
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "VL", "H", "normal"); //532
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "L", "VL", "normal"); //533
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "L", "L", "mb_normal"); //534
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "L", "M", "mb_normal"); //535
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "L", "H", "mb_normal"); //536
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "M", "VL", "normal"); //537
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "M", "L", "mb_normal"); //538
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "M", "M", "mb_anomaly"); //539
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "M", "H", "mb_anomaly"); //540
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "H", "VL", "normal"); //541
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "H", "L", "mb_normal"); //542
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "H", "M", "mb_anomaly"); //543
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "L", "H", "H", "anomaly"); //544
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "VL", "VL", "normal"); //545
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "VL", "L", "normal"); //546
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "VL", "M", "mb_anomaly"); //547
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "VL", "H", "normal"); //548
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "L", "VL", "normal"); //549
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "L", "L", "mb_normal"); //550
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "L", "M", "mb_anomaly"); //551
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "L", "H", "mb_anomaly"); //552
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "M", "VL", "mb_anomaly"); //553
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "M", "L", "mb_anomaly"); //554
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "M", "M", "mb_anomaly"); //555
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "M", "H", "mb_anomaly"); //556
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "H", "VL", "normal"); //557
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "H", "L", "mb_anomaly"); //558
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "H", "M", "mb_anomaly"); //559
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "M", "H", "H", "mb_anomaly"); //560
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "VL", "VL", "normal"); //561
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "VL", "L", "normal"); //562
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "VL", "M", "normal"); //563
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "VL", "H", "normal"); //564
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "L", "VL", "normal"); //565
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "L", "L", "mb_normal"); //566
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "L", "M", "mb_anomaly"); //567
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "L", "H", "anomaly"); //568
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "M", "VL", "normal"); //569
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "M", "L", "mb_anomaly"); //570
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "M", "M", "mb_anomaly"); //571
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "M", "H", "mb_anomaly"); //572
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "H", "VL", "normal"); //573
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "H", "L", "anomaly"); //574
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "H", "M", "mb_anomaly"); //575
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "VL", "H", "H", "H", "anomaly"); //576
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "VL", "VL", "normal"); //577
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "VL", "L", "normal"); //578
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "VL", "M", "normal"); //579
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "VL", "H", "normal"); //580
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "L", "VL", "normal"); //581
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "L", "L", "mb_normal"); //582
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "L", "M", "mb_normal"); //583
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "L", "H", "mb_normal"); //584
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "M", "VL", "normal"); //585
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "M", "L", "mb_normal"); //586
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "M", "M", "mb_anomaly"); //587
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "M", "H", "mb_anomaly"); //588
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "H", "VL", "normal"); //589
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "H", "L", "mb_normal"); //590
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "H", "M", "mb_anomaly"); //591
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "VL", "H", "H", "anomaly"); //592
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "VL", "VL", "normal"); //593
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "VL", "L", "mb_normal"); //594
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "VL", "M", "mb_normal"); //595
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "VL", "H", "mb_normal"); //596
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "L", "VL", "mb_normal"); //597
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "L", "L", "mb_normal"); //598
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "L", "M", "mb_normal"); //599
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "L", "H", "mb_normal"); //600
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "M", "VL", "mb_normal"); //601
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "M", "L", "mb_normal"); //602
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "M", "M", "mb_anomaly"); //603
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "M", "H", "mb_normal"); //604
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "H", "VL", "mb_normal"); //605
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "H", "L", "mb_normal"); //606
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "H", "M", "mb_normal"); //607
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "L", "H", "H", "mb_normal"); //608
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "VL", "VL", "normal"); //609
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "VL", "L", "mb_normal"); //610
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "VL", "M", "mb_anomaly"); //611
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "VL", "H", "mb_anomaly"); //612
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "L", "VL", "mb_normal"); //613
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "L", "L", "mb_normal"); //614
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "L", "M", "mb_anomaly"); //615
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "L", "H", "mb_normal"); //616
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "M", "VL", "mb_anomaly"); //617
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "M", "L", "mb_anomaly"); //618
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "M", "M", "mb_anomaly"); //619
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "M", "H", "mb_anomaly"); //620
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "H", "VL", "mb_anomaly"); //621
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "H", "L", "mb_normal"); //622
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "H", "M", "mb_anomaly"); //623
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "M", "H", "H", "mb_anomaly"); //624
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "VL", "VL", "normal"); //625
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "VL", "L", "mb_normal"); //626
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "VL", "M", "mb_anomaly"); //627
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "VL", "H", "anomaly"); //628
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "L", "VL", "mb_normal"); //629
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "L", "L", "mb_normal"); //630
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "L", "M", "mb_normal"); //631
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "L", "H", "mb_normal"); //632
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "M", "VL", "mb_anomaly"); //633
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "M", "L", "mb_normal"); //634
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "M", "M", "mb_anomaly"); //635
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "M", "H", "mb_anomaly"); //636
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "H", "VL", "anomaly"); //637
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "H", "L", "mb_normal"); //638
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "H", "M", "mb_anomaly"); //639
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "L", "H", "H", "H", "anomaly"); //640
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "VL", "VL", "normal"); //641
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "VL", "L", "normal"); //642
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "VL", "M", "mb_anomaly"); //643
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "VL", "H", "normal"); //644
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "L", "VL", "normal"); //645
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "L", "L", "mb_normal"); //646
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "L", "M", "mb_anomaly"); //647
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "L", "H", "mb_anomaly"); //648
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "M", "VL", "mb_anomaly"); //649
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "M", "L", "mb_anomaly"); //650
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "M", "M", "mb_anomaly"); //651
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "M", "H", "mb_anomaly"); //652
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "H", "VL", "normal"); //653
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "H", "L", "mb_anomaly"); //654
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "H", "M", "mb_anomaly"); //655
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "VL", "H", "H", "mb_anomaly"); //656
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "VL", "VL", "normal"); //657
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "VL", "L", "mb_normal"); //658
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "VL", "M", "mb_anomaly"); //659
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "VL", "H", "mb_anomaly"); //660
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "L", "VL", "mb_normal"); //661
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "L", "L", "mb_normal"); //662
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "L", "M", "mb_anomaly"); //663
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "L", "H", "mb_normal"); //664
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "M", "VL", "mb_anomaly"); //665
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "M", "L", "mb_anomaly"); //666
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "M", "M", "mb_anomaly"); //667
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "M", "H", "mb_anomaly"); //668
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "H", "VL", "mb_anomaly"); //669
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "H", "L", "mb_normal"); //670
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "H", "M", "mb_anomaly"); //671
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "L", "H", "H", "mb_anomaly"); //672
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "VL", "VL", "mb_anomaly"); //673
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "VL", "L", "mb_anomaly"); //674
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "VL", "M", "mb_anomaly"); //675
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "VL", "H", "mb_anomaly"); //676
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "L", "VL", "mb_anomaly"); //677
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "L", "L", "mb_anomaly"); //678
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "L", "M", "mb_anomaly"); //679
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "L", "H", "mb_anomaly"); //680
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "M", "VL", "mb_anomaly"); //681
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "M", "L", "mb_anomaly"); //682
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "M", "M", "mb_anomaly"); //683
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "M", "H", "mb_anomaly"); //684
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "H", "VL", "mb_anomaly"); //685
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "H", "L", "mb_anomaly"); //686
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "H", "M", "mb_anomaly"); //687
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "M", "H", "H", "mb_anomaly"); //688
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "VL", "VL", "normal"); //689
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "VL", "L", "mb_anomaly"); //690
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "VL", "M", "mb_anomaly"); //691
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "VL", "H", "mb_anomaly"); //692
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "L", "VL", "mb_anomaly"); //693
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "L", "L", "mb_normal"); //694
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "L", "M", "mb_anomaly"); //695
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "L", "H", "mb_anomaly"); //696
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "M", "VL", "mb_anomaly"); //697
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "M", "L", "mb_anomaly"); //698
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "M", "M", "mb_anomaly"); //699
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "M", "H", "mb_anomaly"); //700
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "H", "VL", "mb_anomaly"); //701
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "H", "L", "mb_anomaly"); //702
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "H", "M", "mb_anomaly"); //703
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "M", "H", "H", "H", "anomaly"); //704
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "VL", "VL", "normal"); //705
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "VL", "L", "normal"); //706
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "VL", "M", "normal"); //707
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "VL", "H", "normal"); //708
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "L", "VL", "normal"); //709
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "L", "L", "mb_normal"); //710
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "L", "M", "mb_anomaly"); //711
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "L", "H", "anomaly"); //712
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "M", "VL", "normal"); //713
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "M", "L", "mb_anomaly"); //714
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "M", "M", "mb_anomaly"); //715
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "M", "H", "mb_anomaly"); //716
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "H", "VL", "normal"); //717
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "H", "L", "anomaly"); //718
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "H", "M", "mb_anomaly"); //719
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "VL", "H", "H", "anomaly"); //720
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "VL", "VL", "normal"); //721
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "VL", "L", "mb_normal"); //722
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "VL", "M", "mb_anomaly"); //723
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "VL", "H", "anomaly"); //724
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "L", "VL", "mb_normal"); //725
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "L", "L", "mb_normal"); //726
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "L", "M", "mb_normal"); //727
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "L", "H", "mb_normal"); //728
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "M", "VL", "mb_anomaly"); //729
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "M", "L", "mb_normal"); //730
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "M", "M", "mb_anomaly"); //731
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "M", "H", "mb_anomaly"); //732
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "H", "VL", "anomaly"); //733
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "H", "L", "mb_normal"); //734
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "H", "M", "mb_anomaly"); //735
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "L", "H", "H", "anomaly"); //736
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "VL", "VL", "normal"); //737
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "VL", "L", "mb_anomaly"); //738
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "VL", "M", "mb_anomaly"); //739
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "VL", "H", "mb_anomaly"); //740
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "L", "VL", "mb_anomaly"); //741
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "L", "L", "mb_normal"); //742
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "L", "M", "mb_anomaly"); //743
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "L", "H", "mb_anomaly"); //744
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "M", "VL", "mb_anomaly"); //745
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "M", "L", "mb_anomaly"); //746
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "M", "M", "mb_anomaly"); //747
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "M", "H", "mb_anomaly"); //748
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "H", "VL", "mb_anomaly"); //749
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "H", "L", "mb_anomaly"); //750
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "H", "M", "mb_anomaly"); //751
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "M", "H", "H", "anomaly"); //752
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "VL", "VL", "normal"); //753
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "VL", "L", "anomaly"); //754
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "VL", "M", "mb_anomaly"); //755
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "VL", "H", "anomaly"); //756
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "L", "VL", "anomaly"); //757
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "L", "L", "mb_normal"); //758
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "L", "M", "mb_anomaly"); //759
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "L", "H", "anomaly"); //760
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "M", "VL", "mb_anomaly"); //761
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "M", "L", "mb_anomaly"); //762
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "M", "M", "mb_anomaly"); //763
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "M", "H", "anomaly"); //764
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "H", "VL", "anomaly"); //765
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "H", "L", "anomaly"); //766
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "H", "M", "anomaly"); //767
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "M", "H", "H", "H", "H", "anomaly"); //768
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "VL", "VL", "normal"); //769
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "VL", "L", "normal"); //770
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "VL", "M", "normal"); //771
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "VL", "H", "normal"); //772
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "L", "VL", "normal"); //773
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "L", "L", "normal"); //774
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "L", "M", "normal"); //775
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "L", "H", "normal"); //776
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "M", "VL", "normal"); //777
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "M", "L", "normal"); //778
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "M", "M", "normal"); //779
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "M", "H", "normal"); //780
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "H", "VL", "normal"); //781
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "H", "L", "normal"); //782
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "H", "M", "normal"); //783
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "VL", "H", "H", "anomaly"); //784
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "VL", "VL", "normal"); //785
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "VL", "L", "normal"); //786
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "VL", "M", "normal"); //787
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "VL", "H", "normal"); //788
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "L", "VL", "normal"); //789
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "L", "L", "mb_normal"); //790
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "L", "M", "mb_normal"); //791
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "L", "H", "mb_normal"); //792
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "M", "VL", "normal"); //793
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "M", "L", "mb_normal"); //794
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "M", "M", "mb_anomaly"); //795
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "M", "H", "anomaly"); //796
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "H", "VL", "normal"); //797
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "H", "L", "mb_normal"); //798
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "H", "M", "anomaly"); //799
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "L", "H", "H", "anomaly"); //800
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "VL", "VL", "normal"); //801
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "VL", "L", "normal"); //802
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "VL", "M", "normal"); //803
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "VL", "H", "normal"); //804
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "L", "VL", "normal"); //805
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "L", "L", "mb_normal"); //806
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "L", "M", "mb_anomaly"); //807
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "L", "H", "anomaly"); //808
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "M", "VL", "normal"); //809
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "M", "L", "mb_anomaly"); //810
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "M", "M", "mb_anomaly"); //811
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "M", "H", "mb_anomaly"); //812
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "H", "VL", "normal"); //813
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "H", "L", "anomaly"); //814
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "H", "M", "mb_anomaly"); //815
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "M", "H", "H", "anomaly"); //816
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "VL", "VL", "normal"); //817
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "VL", "L", "normal"); //818
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "VL", "M", "normal"); //819
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "VL", "H", "anomaly"); //820
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "L", "VL", "normal"); //821
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "L", "L", "mb_normal"); //822
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "L", "M", "anomaly"); //823
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "L", "H", "anomaly"); //824
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "M", "VL", "normal"); //825
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "M", "L", "anomaly"); //826
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "M", "M", "mb_anomaly"); //827
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "M", "H", "anomaly"); //828
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "H", "VL", "anomaly"); //829
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "H", "L", "anomaly"); //830
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "H", "M", "anomaly"); //831
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "VL", "H", "H", "H", "anomaly"); //832
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "VL", "VL", "normal"); //833
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "VL", "L", "normal"); //834
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "VL", "M", "normal"); //835
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "VL", "H", "normal"); //836
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "L", "VL", "normal"); //837
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "L", "L", "mb_normal"); //838
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "L", "M", "mb_normal"); //839
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "L", "H", "mb_normal"); //840
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "M", "VL", "normal"); //841
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "M", "L", "mb_normal"); //842
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "M", "M", "mb_anomaly"); //843
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "M", "H", "anomaly"); //844
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "H", "VL", "normal"); //845
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "H", "L", "mb_normal"); //846
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "H", "M", "anomaly"); //847
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "VL", "H", "H", "anomaly"); //848
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "VL", "VL", "normal"); //849
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "VL", "L", "mb_normal"); //850
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "VL", "M", "mb_normal"); //851
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "VL", "H", "mb_normal"); //852
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "L", "VL", "mb_normal"); //853
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "L", "L", "mb_normal"); //854
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "L", "M", "mb_normal"); //855
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "L", "H", "mb_normal"); //856
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "M", "VL", "mb_normal"); //857
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "M", "L", "mb_normal"); //858
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "M", "M", "mb_normal"); //859
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "M", "H", "mb_normal"); //860
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "H", "VL", "mb_normal"); //861
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "H", "L", "mb_normal"); //862
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "H", "M", "mb_normal"); //863
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "L", "H", "H", "anomaly"); //864
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "VL", "VL", "normal"); //865
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "VL", "L", "mb_normal"); //866
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "VL", "M", "mb_anomaly"); //867
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "VL", "H", "anomaly"); //868
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "L", "VL", "mb_normal"); //869
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "L", "L", "mb_normal"); //870
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "L", "M", "mb_normal"); //871
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "L", "H", "mb_normal"); //872
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "M", "VL", "mb_anomaly"); //873
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "M", "L", "mb_normal"); //874
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "M", "M", "mb_anomaly"); //875
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "M", "H", "mb_anomaly"); //876
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "H", "VL", "anomaly"); //877
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "H", "L", "mb_normal"); //878
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "H", "M", "mb_anomaly"); //879
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "M", "H", "H", "anomaly"); //880
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "VL", "VL", "normal"); //881
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "VL", "L", "mb_normal"); //882
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "VL", "M", "anomaly"); //883
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "VL", "H", "anomaly"); //884
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "L", "VL", "mb_normal"); //885
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "L", "L", "mb_normal"); //886
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "L", "M", "mb_normal"); //887
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "L", "H", "anomaly"); //888
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "M", "VL", "anomaly"); //889
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "M", "L", "mb_normal"); //890
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "M", "M", "mb_anomaly"); //891
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "M", "H", "anomaly"); //892
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "H", "VL", "anomaly"); //893
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "H", "L", "anomaly"); //894
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "H", "M", "anomaly"); //895
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "L", "H", "H", "H", "anomaly"); //896
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "VL", "VL", "normal"); //897
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "VL", "L", "normal"); //898
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "VL", "M", "normal"); //899
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "VL", "H", "normal"); //900
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "L", "VL", "normal"); //901
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "L", "L", "mb_normal"); //902
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "L", "M", "mb_anomaly"); //903
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "L", "H", "anomaly"); //904
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "M", "VL", "normal"); //905
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "M", "L", "mb_anomaly"); //906
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "M", "M", "mb_anomaly"); //907
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "M", "H", "mb_anomaly"); //908
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "H", "VL", "normal"); //909
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "H", "L", "anomaly"); //910
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "H", "M", "mb_anomaly"); //911
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "VL", "H", "H", "anomaly"); //912
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "VL", "VL", "normal"); //913
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "VL", "L", "mb_normal"); //914
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "VL", "M", "mb_anomaly"); //915
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "VL", "H", "anomaly"); //916
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "L", "VL", "mb_normal"); //917
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "L", "L", "mb_normal"); //918
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "L", "M", "mb_normal"); //919
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "L", "H", "mb_normal"); //920
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "M", "VL", "mb_anomaly"); //921
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "M", "L", "mb_normal"); //922
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "M", "M", "mb_anomaly"); //923
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "M", "H", "mb_anomaly"); //924
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "H", "VL", "anomaly"); //925
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "H", "L", "mb_normal"); //926
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "H", "M", "mb_anomaly"); //927
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "L", "H", "H", "anomaly"); //928
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "VL", "VL", "normal"); //929
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "VL", "L", "mb_anomaly"); //930
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "VL", "M", "mb_anomaly"); //931
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "VL", "H", "mb_anomaly"); //932
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "L", "VL", "mb_anomaly"); //933
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "L", "L", "mb_normal"); //934
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "L", "M", "mb_anomaly"); //935
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "L", "H", "mb_anomaly"); //936
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "M", "VL", "mb_anomaly"); //937
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "M", "L", "mb_anomaly"); //938
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "M", "M", "mb_anomaly"); //939
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "M", "H", "mb_anomaly"); //940
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "H", "VL", "mb_anomaly"); //941
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "H", "L", "mb_anomaly"); //942
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "H", "M", "mb_anomaly"); //943
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "M", "H", "H", "anomaly"); //944
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "VL", "VL", "normal"); //945
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "VL", "L", "anomaly"); //946
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "VL", "M", "mb_anomaly"); //947
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "VL", "H", "anomaly"); //948
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "L", "VL", "anomaly"); //949
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "L", "L", "mb_normal"); //950
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "L", "M", "mb_anomaly"); //951
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "L", "H", "anomaly"); //952
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "M", "VL", "mb_anomaly"); //953
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "M", "L", "mb_anomaly"); //954
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "M", "M", "mb_anomaly"); //955
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "M", "H", "anomaly"); //956
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "H", "VL", "anomaly"); //957
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "H", "L", "anomaly"); //958
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "H", "M", "anomaly"); //959
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "M", "H", "H", "H", "anomaly"); //960
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "VL", "VL", "normal"); //961
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "VL", "L", "normal"); //962
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "VL", "M", "normal"); //963
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "VL", "H", "anomaly"); //964
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "L", "VL", "normal"); //965
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "L", "L", "mb_normal"); //966
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "L", "M", "anomaly"); //967
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "L", "H", "anomaly"); //968
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "M", "VL", "normal"); //969
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "M", "L", "anomaly"); //970
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "M", "M", "mb_anomaly"); //971
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "M", "H", "anomaly"); //972
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "H", "VL", "anomaly"); //973
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "H", "L", "anomaly"); //974
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "H", "M", "anomaly"); //975
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "VL", "H", "H", "anomaly"); //976
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "VL", "VL", "normal"); //977
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "VL", "L", "mb_normal"); //978
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "VL", "M", "anomaly"); //979
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "VL", "H", "anomaly"); //980
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "L", "VL", "mb_normal"); //981
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "L", "L", "mb_normal"); //982
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "L", "M", "mb_normal"); //983
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "L", "H", "anomaly"); //984
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "M", "VL", "anomaly"); //985
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "M", "L", "mb_normal"); //986
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "M", "M", "mb_anomaly"); //987
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "M", "H", "anomaly"); //988
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "H", "VL", "anomaly"); //989
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "H", "L", "anomaly"); //990
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "H", "M", "anomaly"); //991
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "L", "H", "H", "anomaly"); //992
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "VL", "VL", "normal"); //993
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "VL", "L", "anomaly"); //994
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "VL", "M", "mb_anomaly"); //995
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "VL", "H", "anomaly"); //996
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "L", "VL", "anomaly"); //997
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "L", "L", "mb_normal"); //998
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "L", "M", "mb_anomaly"); //999
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "L", "H", "anomaly"); //1000
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "M", "VL", "mb_anomaly"); //1001
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "M", "L", "mb_anomaly"); //1002
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "M", "M", "mb_anomaly"); //1003
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "M", "H", "anomaly"); //1004
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "H", "VL", "anomaly"); //1005
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "H", "L", "anomaly"); //1006
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "H", "M", "anomaly"); //1007
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "M", "H", "H", "anomaly"); //1008
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "VL", "VL", "anomaly"); //1009
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "VL", "L", "anomaly"); //1010
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "VL", "M", "anomaly"); //1011
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "VL", "H", "anomaly"); //1012
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "L", "VL", "anomaly"); //1013
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "L", "L", "anomaly"); //1014
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "L", "M", "anomaly"); //1015
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "L", "H", "anomaly"); //1016
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "M", "VL", "anomaly"); //1017
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "M", "L", "anomaly"); //1018
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "M", "M", "anomaly"); //1019
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "M", "H", "anomaly"); //1020
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "H", "VL", "anomaly"); //1021
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "H", "L", "anomaly"); //1022
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "H", "M", "anomaly"); //1023
                AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAnomaly, "H", "H", "H", "H", "H", "anomaly"); //1024

            }
            catch (Exception ex)
            {
                MessageBox.Show(string.Format("Parsing exception: {0}", ex.Message));
                return null;
            }

            return fsSugeno;
        }

        void AddSugenoFuzzyRule(
            SugenoFuzzySystem fs,
            FuzzyVariable fv1,
            FuzzyVariable fv2,
            FuzzyVariable fv3,
            FuzzyVariable fv4,
            FuzzyVariable fv5,
            SugenoVariable sv,
            string value1,
            string value2,
            string value3,
            string value4,
            string value5,
            string result)
        {
            SugenoFuzzyRule rule = fs.EmptyRule();
            rule.Condition.Op = OperatorType.And;
            rule.Condition.ConditionsList.Add(rule.CreateCondition(fv1, fv1.GetTermByName(value1)));
            rule.Condition.ConditionsList.Add(rule.CreateCondition(fv2, fv2.GetTermByName(value2)));
            rule.Condition.ConditionsList.Add(rule.CreateCondition(fv3, fv3.GetTermByName(value3)));
            rule.Condition.ConditionsList.Add(rule.CreateCondition(fv4, fv4.GetTermByName(value4)));
            rule.Condition.ConditionsList.Add(rule.CreateCondition(fv5, fv5.GetTermByName(value5)));
            rule.Conclusion.Var = sv;
            rule.Conclusion.Term = sv.GetFuncByName(result);
            fs.Rules.Add(rule);
        }
        private string ToLinguistic(double value)
        {
            string str = "ERROR";


            if (value <= 0.3) str = "normal";
            if (value >= 0.2 && value <= 0.6) str = "mb_normal";
            if (value >= 0.5 && value <= 0.8) str = "mb_anomaly";
            if (value >= 0.7) str = "anomaly";
            if (value >= 0 && value <= 0.000099) str = "anomaly";

            return str; 
        }

        private string Pointer(string value)
        {
            if (value.IndexOf(',')>-1) value = value.Replace(',','.');
            return value;
        }

        private string Procent(int value_1, int value_2)
        {
            string str = Convert.ToString(Convert.ToDouble(value_1)/value_2) + " %";
            return str;
        }

        public string Start(string path, int type_output_result, int create_new_DB, string path_new_DB_FL, int type_of_association)
        {
            string result_str = "";
            int counter = 0;
            string atr = "";
            string line;
            StreamReader sr = new StreamReader(path);
            while ((line = sr.ReadLine()) != "@data")
            {
                line = line.Replace("@attribute ", "");
                atr += line;
                counter++;
            }

            if ((atr.IndexOf("src_bytes") < 0) || (atr.IndexOf("dst_bytes") < 0) || (atr.IndexOf("serror_rate") < 0) || (atr.IndexOf("dst_host_serror_rate") < 0) || (atr.IndexOf("dst_host_srv_serror_rate") < 0))
            {
                result_str = "В базе данных нет необходимых атрибутов!";
            }
            else
            {
                atr = atr.Replace("@relation '", "");
                atr = atr.Substring(atr.IndexOf("\'") + 2);
                atr = atr.Replace("'0'", "");
                atr = atr.Replace("'1'", "");
                atr = atr.Replace(" ", "");
                atr = atr.Replace("real", "");
                atr = atr.Replace("@data", "");
                while (atr.IndexOf("}") > -1)
                {
                    atr = atr.Remove(atr.IndexOf("{"), atr.IndexOf("}") - atr.IndexOf("{") + 1);
                }
                atr = atr.Replace("\'\'", ",");
                atr = atr.Replace("\'", "");
                string[] atr_mas_mame = new string[90];
                atr_mas_mame = atr.Split(',');

               
                if (create_new_DB == 1)
                {
                    streamWriter_FL = new StreamWriter(path_new_DB_FL, true);
                    
                    string output_in_file_str = "";

                    string[] atr_name_mas_full = { "duration", "protocol_type", "service", "flag", "src_bytes", "dst_bytes", "land", "wrong_fragment", "urgent", "hot", "num_failed_logins", "logged_in", "num_compromised", "root_shell", "su_attempted", "num_root", "num_file_creations", "num_shells", "num_access_files", "num_outbound_cmds", "is_host_login", "is_guest_login", "count", "srv_count", "serror_rate", "srv_serror_rate", "rerror_rate", "srv_rerror_rate", "same_srv_rate", "diff_srv_rate", "srv_diff_host_rate", "dst_host_count", "dst_host_srv_count", "dst_host_same_srv_rate", "dst_host_diff_srv_rate", "dst_host_same_src_port_rate", "dst_host_srv_diff_host_rate", "dst_host_serror_rate", "dst_host_srv_serror_rate", "dst_host_rerror_rate", "dst_host_srv_rerror_rate", "class_normal_anomaly" };
                    output_in_file_str = "@relation 'ALL' \r\n";

                    for (int i = 0; i < atr_mas_mame.Length - 1; i++)
                    {
                        output_in_file_str += Interaction_with_DB.attributeMas[Array.IndexOf(atr_name_mas_full, atr_mas_mame[i])] + "\r\n";

                    }

                    output_in_file_str += "@attribute \'fuzzy_logic_class\' {\'normal\', \'mb_normal\', \'mb_anomaly\', \'anomaly\'}\r\n";
                    output_in_file_str += Interaction_with_DB.attributeMas[Array.IndexOf(atr_name_mas_full, atr_mas_mame[atr_mas_mame.Length - 1])] + "\r\n";
                    output_in_file_str += "@data \r\n";

                    streamWriter_FL.Write(output_in_file_str);
                  

                }

                //--------------------------- Создание новой нечеткой системы ---------------------------------------------

                if (_fsSugeno == null)
                {
                    _fsSugeno = CreateSystem();
                    if (_fsSugeno == null)
                    {
                        result_str = "Не удалось создать новую нечёткую систему!";
                    }

                    //------------------- Получить переменные из системы (только для удобства) --------------------------------

                    FuzzyVariable fvSrc_bytes = _fsSugeno.InputByName("Src_bytes");
                    FuzzyVariable fvDst_bytes = _fsSugeno.InputByName("Dst_bytes");
                    FuzzyVariable fvSerror_rate = _fsSugeno.InputByName("Serror_rate");
                    FuzzyVariable fvDst_host_serror_rate = _fsSugeno.InputByName("Dst_host_serror_rate");
                    FuzzyVariable fvDst_host_srv_serror_rate = _fsSugeno.InputByName("Dst_host_srv_serror_rate");
                    SugenoVariable svAnomaly = _fsSugeno.OutputByName("Anomaly");

                    //---------------------------------- Получение данных -----------------------------------------------------
                    string[] atr_mas = new string[counter];
                    double[] temp_mas = new double[6];
                    int numb_str = 0;

                    int[] temp_mas_index = new int[6];
                    temp_mas_index[0] = Array.IndexOf(atr_mas_mame, "src_bytes");
                    temp_mas_index[1] = Array.IndexOf(atr_mas_mame, "dst_bytes");
                    temp_mas_index[2] = Array.IndexOf(atr_mas_mame, "serror_rate");
                    temp_mas_index[3] = Array.IndexOf(atr_mas_mame, "dst_host_serror_rate");
                    temp_mas_index[4] = Array.IndexOf(atr_mas_mame, "dst_host_srv_serror_rate");
                    temp_mas_index[5] = Array.IndexOf(atr_mas_mame, "class_normal_anomaly");


                    if (type_output_result == 1) result_str = "№, Src_bytes, Dst_bytes, Serror_rate, Dst_host_serror_rate, Dst_host_srv_serror_rate, Tag, Result\r\n";
                    if (type_output_result == 2) result_str = "Оригинальная запись, результат работы\r\n";


                    int absolutely = 0, half = 0, err1 = 0, err2 = 0;
                    int trueNormal = 0, falseNormal = 0, trueAnomaly = 0, falseAnomaly = 0, trueMbNormal = 0, falseMbNormal = 0, trueMbAnomaly = 0, falseMbAnomaly = 0;
                    string atrClass = "";
                    while ((line = sr.ReadLine()) != null)
                    {
                        numb_str++;
                        atr_mas = line.Split(',');

                        if (atr_mas[temp_mas_index[0]].IndexOf(".") > -1) { atr_mas[temp_mas_index[0]] = atr_mas[temp_mas_index[0]].Replace(".", ","); }
                        else { atr_mas[temp_mas_index[0]] += ",0"; }
                        if (atr_mas[temp_mas_index[1]].IndexOf(".") > -1) { atr_mas[temp_mas_index[1]] = atr_mas[temp_mas_index[1]].Replace(".", ","); }
                        else { atr_mas[temp_mas_index[1]] += ",0"; }
                        if (atr_mas[temp_mas_index[2]].IndexOf(".") > -1) { atr_mas[temp_mas_index[2]] = atr_mas[temp_mas_index[2]].Replace(".", ","); }
                        else { atr_mas[temp_mas_index[2]] += ",0"; }
                        if (atr_mas[temp_mas_index[3]].IndexOf(".") > -1) { atr_mas[temp_mas_index[3]] = atr_mas[temp_mas_index[3]].Replace(".", ","); }
                        else { atr_mas[temp_mas_index[3]] += ",0"; }
                        if (atr_mas[temp_mas_index[4]].IndexOf(".") > -1) { atr_mas[temp_mas_index[4]] = atr_mas[temp_mas_index[4]].Replace(".", ","); }
                        else { atr_mas[temp_mas_index[4]] += ",0"; }

                        temp_mas[0] = Convert.ToDouble(atr_mas[temp_mas_index[0]]) / 100000;
                        temp_mas[1] = Convert.ToDouble(atr_mas[temp_mas_index[1]]) / 100000;
                        temp_mas[2] = Convert.ToDouble(atr_mas[temp_mas_index[2]]);
                        temp_mas[3] = Convert.ToDouble(atr_mas[temp_mas_index[3]]);
                        temp_mas[4] = Convert.ToDouble(atr_mas[temp_mas_index[4]]);

                        atrClass = atr_mas[temp_mas_index[5]];


                        //---------------------------------- Нечёткий ввод данных -----------------------------------------------------

                        Dictionary<FuzzyVariable, double> inputValues = new Dictionary<FuzzyVariable, double>();
                        inputValues.Add(fvSrc_bytes, temp_mas[0]);
                        inputValues.Add(fvDst_bytes, temp_mas[1]);
                        inputValues.Add(fvSerror_rate, temp_mas[2]);
                        inputValues.Add(fvDst_host_serror_rate, temp_mas[3]);
                        inputValues.Add(fvDst_host_srv_serror_rate, temp_mas[4]);

                        //---------------------------------- Вычисление результата -----------------------------------------------------

                        Dictionary<SugenoVariable, double> result = _fsSugeno.Calculate(inputValues);

                        outResultNumb = result[svAnomaly];
                        string resultStr = ToLinguistic(result[svAnomaly]);

                        if (atrClass == resultStr) { absolutely++; half++; err1++; err2++; }
                        if (atrClass == "normal" && resultStr == "mb_normal") { half++; err2++; }
                        if (atrClass == "anomaly" && resultStr == "mb_anomaly") { half++; err1++; }
                        if (atrClass == "anomaly" && resultStr == "mb_normal") { err1++; }
                        if (atrClass == "normal" && resultStr == "mb_anomaly") { err2++; }

                        if (atrClass == "normal" && resultStr == "normal") { trueNormal++; }
                        if (atrClass == "normal" && resultStr != "normal") { falseNormal++; }
                        if (atrClass == "anomaly" && resultStr == "anomaly") { trueAnomaly++; }
                        if (atrClass == "anomaly" && resultStr != "anomaly") { falseAnomaly++; }
                        if (atrClass == "normal" && resultStr == "mb_normal") { trueMbNormal++; }
                        if (atrClass == "anomaly" && resultStr == "mb_normal") { falseMbNormal++; }
                        if (atrClass == "anomaly" && resultStr == "mb_anomaly") { trueMbAnomaly++; }
                        if (atrClass == "normal" && resultStr == "mb_anomaly") { falseMbAnomaly++; }
                       

                        if (type_output_result == 1) result_str += numb_str + ", " + Pointer((temp_mas[0] * 100000).ToString()) + ", " + Pointer((temp_mas[1] * 100000).ToString()) + ", " + Pointer(temp_mas[2].ToString()) + ", " + Pointer(temp_mas[3].ToString()) + ", " + Pointer(temp_mas[4].ToString()) + ", " + atrClass + ", " + resultStr + ", " + outResultNumb + "  \r\n";
                        if (type_output_result == 2)
                        {
                            for (int i = 0; i < counter - 1; i++) { result_str += Pointer(atr_mas[i]) + ","; }
                            result_str += resultStr + "\r\n";
                        }
                        if (create_new_DB == 1) {
                           
                            string output_in_file_str = "";
                            output_in_file_str = "";
                            for (int i = 0; i < counter - 2; i++) { output_in_file_str += Pointer(atr_mas[i]) + ","; }
                            switch (type_of_association)
                            {
                                case 0:
                                    output_in_file_str += resultStr + ",";
                                    break;

                                case 1:
                                    if (resultStr == "normal" || resultStr == "anomaly") { output_in_file_str += resultStr + ","; break; }
                                    if (resultStr == "mb_normal") { output_in_file_str += "normal" + ","; break; }
                                    if (resultStr == "mb_anomaly") { output_in_file_str += "anomaly" + ","; break; } 
                                    break;

                                case 2:
                                    if (resultStr == "normal" || resultStr == "anomaly") { output_in_file_str += resultStr + ","; break; }
                                    if (resultStr == "mb_normal") { output_in_file_str += "anomaly" + ","; break; }
                                    if (resultStr == "mb_anomaly") { output_in_file_str += "anomaly" + ","; break; }
                                    break;

                                case 3:
                                    if (resultStr == "normal" || resultStr == "anomaly") { output_in_file_str += resultStr + ","; break; }
                                    if (resultStr == "mb_normal") { output_in_file_str += "normal" + ","; break; }
                                    if (resultStr == "mb_anomaly") { output_in_file_str += "normal" + ","; break; }
                                    break;
                            }

                            output_in_file_str += atr_mas[counter - 2] + "\r\n";
                            streamWriter_FL.Write(output_in_file_str);
                           
                        }
                    }

                    if (create_new_DB == 1) streamWriter_FL.Close();
                    result_str += "Завершён поток с набором данных:" + path + "\r\n\r\n";
                    result_str += "Количество правильно классифицированных записей \r\n";
                    result_str += "Учитывается только абсолютное совпадение с меткой из БД - " + absolutely + " из " + numb_str + ",   " + Procent(absolutely, numb_str) + "\r\n";
                    result_str += "mb_normal относится к нормальным данным, а mb_ anomaly к аномалии - " + half + " из " + numb_str + ",   " + Procent(half, numb_str) + "\r\n";
                    result_str += "mb_normal и mb_ anomaly относятся к аномалии - " + err1 + " из " + numb_str + ",   " + Procent(err1, numb_str) + "\r\n";
                    result_str += "mb_normal и mb_ anomaly относятся к нормальным данным - " + err2 + " из " + numb_str + ",   " + Procent(err2, numb_str) + "\r\n";


                    result_str += "Количество правильно классифицированных записей нормальных данных:" + trueNormal + "\r\n";
                    result_str += "Количество неправильно классифицированных записей нормальных данных:" + falseNormal + "\r\n\r\n";
                    result_str += "Количество правильно классифицированных записей данных атак:" + trueAnomaly + "\r\n";
                    result_str += "Количество неправильно классифицированных записей данных атак:" + falseAnomaly + "\r\n\r\n";
                    result_str += "Количество правильно классифицированных записей может быть нормальных данных:" + trueMbNormal + "\r\n";
                    result_str += "Количество неправильно классифицированных записей может быть нормальных данных:" + falseMbNormal + "\r\n\r\n";
                    result_str += "Количество правильно классифицированных записей может быть данных атак:" + trueMbAnomaly + "\r\n";
                    result_str += "Количество неправильно классифицированных записей может быть данных атак:" + falseMbAnomaly + "\r\n\r\n";

                }
            }

            
            return result_str;
        }
    }
}

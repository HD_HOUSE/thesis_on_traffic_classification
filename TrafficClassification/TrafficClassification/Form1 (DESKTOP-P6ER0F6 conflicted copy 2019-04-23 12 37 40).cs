﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
//-----------------------------------
using weka.classifiers;
using weka.classifiers.evaluation;
using weka.core.converters;
using weka.filters;
using weka.filters.unsupervised.attribute;


namespace TrafficClassification
{

  

    public partial class Form1 : Form
    {
        private weka.core.Instances trainingDataSet_1DB;
        private weka.core.Instances testingDataSet_1DB;

        List<int> featuresMas = new List<int>();
        public string Name_DB_creating_str = "";
        public int numbTraining_const = 0;
        public int type_classification = 0;

        public Form1()
        {
            InitializeComponent();

        }

        public void AppendProgressBarDB(int value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<int>(AppendProgressBarDB), new object[] { value });
                return;
            }
            progressBar1.Value = value;
        }

        public void AppendTextBox(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(AppendTextBox), new object[] { value });
                return;
            }
            textBox_resultOut.Text += value;
        }

        public void AppendTextBox_2(string value)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(AppendTextBox_2), new object[] { value });
                return;
            }
            textBox_resultOut_2.Text += value;
        }

        private void Form1_Load(object sender, EventArgs e)
        {

            comboBox_type_DB.SelectedIndex = 1;
            comboBox_type_classification.SelectedIndex = 0;
            radioButton1.Checked = true;
           

        }

        //-----------------------Вкладка подготовка базы данных-----------------------------------------------------------------------------




        private void button3_Click(object sender, EventArgs e)
        {
            if (Name_DB_creating.Text != "")
            {
                AppendProgressBarDB(0);
                featuresMas.Clear();

                for (int i = 0; i <= (features.Items.Count - 1); i++)
                {
                    if (features.GetItemChecked(i))
                    {
                        featuresMas.Add(i + 1);
                    }
                }

                Name_DB_creating_str = Name_DB_creating.Text;
                numbTraining_const = Convert.ToInt32(textBox_numbTraining.Text);
                type_classification = comboBox_type_classification.SelectedIndex;


                if (comboBox_type_DB.SelectedIndex == 0)
                {
                    if (Convert.ToInt32(textBox_numbTraining.Text) < 148517)
                    {
                        Thread DB2_Thread = new Thread(new ThreadStart(DB2));
                        DB2_Thread.Start(); // запускаем поток
                    }
                    else MessageBox.Show("Введённое число записей больше чем есть в БД!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

                if (comboBox_type_DB.SelectedIndex == 1)
                {
                    Thread DB1_Thread = new Thread(new ThreadStart(DB1));
                    DB1_Thread.Start(); // запускаем поток
                }
            }
            else MessageBox.Show("Не заполнено поле 'Название'!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);

        }


        private void DB2()
        {
            Interaction_with_DB DB = new Interaction_with_DB();
            DB.create_2_DB(type_classification, featuresMas, numbTraining_const, Name_DB_creating_str);
        }

        private void DB1()
        {
            Interaction_with_DB DB = new Interaction_with_DB();
            DB.create_1_DB(type_classification, featuresMas, numbTraining_const, Name_DB_creating_str);
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            for (int i = 0; i < features.Items.Count; i++)
            {
                features.SetItemChecked(i, true);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            features.SetItemChecked(0, false);
            features.SetItemChecked(1, false);
            features.SetItemChecked(2, true);
            features.SetItemChecked(3, true);
            features.SetItemChecked(4, true);
            features.SetItemChecked(5, true);
            features.SetItemChecked(6, false);
            features.SetItemChecked(7, true);
            features.SetItemChecked(8, false);
            features.SetItemChecked(9, false);
            features.SetItemChecked(10, false);
            features.SetItemChecked(11, true);
            features.SetItemChecked(12, false);
            features.SetItemChecked(13, false);
            features.SetItemChecked(14, false);
            features.SetItemChecked(15, false);
            features.SetItemChecked(16, false);
            features.SetItemChecked(17, false);
            features.SetItemChecked(18, false);
            features.SetItemChecked(19, false);
            features.SetItemChecked(20, false);
            features.SetItemChecked(21, false);
            features.SetItemChecked(22, true);
            features.SetItemChecked(23, false);
            features.SetItemChecked(24, true);
            features.SetItemChecked(25, true);
            features.SetItemChecked(26, false);
            features.SetItemChecked(27, false);
            features.SetItemChecked(28, true);
            features.SetItemChecked(29, true);
            features.SetItemChecked(30, true);
            features.SetItemChecked(31, false);
            features.SetItemChecked(32, true);
            features.SetItemChecked(33, true);
            features.SetItemChecked(34, true);
            features.SetItemChecked(35, false);
            features.SetItemChecked(36, true);
            features.SetItemChecked(37, true);
            features.SetItemChecked(38, true);
            features.SetItemChecked(39, false);
            features.SetItemChecked(40, true);

        }

        private void comboBox_type_DB_SelectionChangeCommitted(object sender, EventArgs e)
        {
            if (comboBox_type_DB.SelectedIndex == 0) textBox_numbTraining.Enabled = true;
            if (comboBox_type_DB.SelectedIndex == 1) textBox_numbTraining.Enabled = false;
        }



        //-----------------------Вкладка единая база-----------------------------------------------------------------------------

        private void button1_Click(object sender, EventArgs e)
        {
            if (0 < Convert.ToInt16(this.textBox_percentSplit.Text) && (Convert.ToInt16(this.textBox_percentSplit.Text) < 100))
            {
               

                if (this.textBox_pathDB.Text == "") MessageBox.Show("Не была выбрана база данных!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    if (checkBox_1DB_RF.Checked || checkBox_1DB_C45.Checked || checkBox_1DB_SVM.Checked) ClassifyTest_1DB(); 
                    else MessageBox.Show("Не был выбран ни один из методов машинного обучения!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }

            }
            else MessageBox.Show("Введён некорректный размер обучающей выборки");
        }




        private void textBox_percentSplit_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;
            if (!Char.IsDigit(number) && number != 8 && number != 44) // цифры, клавиша BackSpace и запятая
            {
                e.Handled = true;
            }
        }


        private void button_add_pathDB_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.textBox_pathDB.Text = openFileDialog1.FileName;
            }
        }

        public void ClassifyTest_1DB()
        {
           // this.textBox_resultOut.Text = "";

            weka.core.Instances instsDB = new weka.core.Instances(new java.io.FileReader(this.textBox_pathDB.Text));
            instsDB.setClassIndex(instsDB.numAttributes() - 1);

            if (radioButton1.Checked)
            {
                weka.filters.Filter myRandomFilters = new weka.filters.unsupervised.instance.Randomize();
                myRandomFilters.setInputFormat(instsDB);
                instsDB = weka.filters.Filter.useFilter(instsDB, myRandomFilters);

                int trainSizeDB = instsDB.numInstances() * Convert.ToInt16(this.textBox_percentSplit.Text) / 100;
                int testSizeDB = instsDB.numInstances() - trainSizeDB;
                trainingDataSet_1DB = new weka.core.Instances(instsDB, 0, trainSizeDB);
                trainingDataSet_1DB.setClassIndex(trainingDataSet_1DB.numAttributes() - 1);
                testingDataSet_1DB = new weka.core.Instances(instsDB, 0, instsDB.numInstances());// сюда попадают данные из обучающей выборки, сделал так пока для теста
                testingDataSet_1DB.setClassIndex(testingDataSet_1DB.numAttributes() - 1);
            }


            if (radioButton2.Checked)
            {
                int trainSizeDB = instsDB.numInstances() * Convert.ToInt16(this.textBox_percentSplit.Text) / 100;
                int testSizeDB = instsDB.numInstances() - trainSizeDB;
                trainingDataSet_1DB = new weka.core.Instances(instsDB, 0, trainSizeDB);
                trainingDataSet_1DB.setClassIndex(trainingDataSet_1DB.numAttributes() - 1);
                                
                testingDataSet_1DB = new weka.core.Instances(instsDB, trainSizeDB, instsDB.numInstances()- trainSizeDB);
                testingDataSet_1DB.setClassIndex(testingDataSet_1DB.numAttributes() - 1);
            }


            if (checkBox_1DB_RF.Checked)
            {
                Thread RF_1DB_Thread = new Thread(new ThreadStart(RF_1DB));
                RF_1DB_Thread.Start(); // запускаем поток
            }

            if (checkBox_1DB_C45.Checked)
            {
                Thread C4_5_1DB_Thread = new Thread(new ThreadStart(C4_5_1DB));
                C4_5_1DB_Thread.Start(); // запускаем поток
            }

            if (checkBox_1DB_SVM.Checked)
            {
                Thread SVM_1DB_Thread = new Thread(new ThreadStart(SVM_1DB));
                SVM_1DB_Thread.Start(); // запускаем поток
            }
        

        }

        public void RF_1DB()
        {
            string Name_Exp = this.textBox_Name_Exp_1.Text;
            сlassificationResults results = new сlassificationResults();
            Classifiers methodML = new Classifiers();

            results = methodML.RandomForest(trainingDataSet_1DB, testingDataSet_1DB, 10);
            PrintResults_1DB(results, Name_Exp);
        }

        public void C4_5_1DB()
        {
            string Name_Exp = this.textBox_Name_Exp_1.Text;
            сlassificationResults results = new сlassificationResults();
            Classifiers methodML = new Classifiers();

            results = methodML.C4_5(trainingDataSet_1DB, testingDataSet_1DB);
            PrintResults_1DB(results, Name_Exp);

        }

        public void SVM_1DB()
        {
            string Name_Exp = this.textBox_Name_Exp_1.Text;
            сlassificationResults results = new сlassificationResults();
            Classifiers methodML = new Classifiers();

            results = methodML.SVM(trainingDataSet_1DB, testingDataSet_1DB);
            PrintResults_1DB(results, Name_Exp);

        }

        public void PrintResults_1DB(сlassificationResults results, string Name_Exp)
        {
            string resultStr = "The name of the experiment " + Name_Exp + "\r\n";
            resultStr += "Algorithm Name: " + results.nameMetods + "\r\n \r\n  ";
            resultStr += "Procent: " + textBox_percentSplit.Text + "\r\n \r\n  ";
            resultStr += results.summary + "\r\n\r\n ";
            resultStr += results.classDetails + "\r\n\r\n ";
            resultStr += "Training time: " + results.timeTraining + "  milliseconds\r\n ";
            resultStr += "Testing time: " + results.timeTesting + "  milliseconds\r\n ";
            resultStr += results.confusionMatrixString + "\r\n\r\n ";    
            resultStr += "Normal       Anomaly \r\n ";
            resultStr += results.confusionMatrix[0][0] + "          " + results.confusionMatrix[0][1] + "\r\n ";
            resultStr += results.confusionMatrix[1][0] + "          " + results.confusionMatrix[1][1] + "\r\n \r\n ";
            resultStr += "\r\n \r\n ";
            AppendTextBox(resultStr);


        }

        //-----------------------Вкладка разделённая база-----------------------------------------------------------------------------

        private void button2_Click_1(object sender, EventArgs e)
        {
            if (this.textBox_pathDB2_train.Text == "") MessageBox.Show("Не была выбрана база данных обучения!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                if (this.textBox_pathDB2_test.Text == "") MessageBox.Show("Не была выбрана база данных тестирования!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    if (checkBox_RF.Checked || checkBox_C4_5.Checked || checkBox_SVM.Checked) ClassifyTest_2DB();
                    else MessageBox.Show("Не был выбран ни один из методов машинного обучения!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        private void button_add_pathDB2_train_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.textBox_pathDB2_train.Text = openFileDialog1.FileName;
            }
        }

        private void button_add_pathDB2_test_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.textBox_pathDB2_test.Text = openFileDialog1.FileName;
            }
        }

        public void ClassifyTest_2DB()
        {
           // this.textBox_resultOut_2.Text = "";
            if (checkBox_RF.Checked)
            {
                Thread RF_Thread = new Thread(new ThreadStart(RF));
                RF_Thread.Start(); // запускаем поток
            }

            if (checkBox_C4_5.Checked)
            {
                Thread C4_5_Thread = new Thread(new ThreadStart(C4_5));
                C4_5_Thread.Start(); // запускаем поток
            }

            if (checkBox_SVM.Checked)
            {
                Thread SVM_Thread = new Thread(new ThreadStart(SVM));
                SVM_Thread.Start(); // запускаем поток
            }

    }

        public void RF()
        {
            string Name_Exp = this.textBox_Name_Exp_2.Text;
            weka.core.Instances trainingDataSet = new weka.core.Instances(new java.io.FileReader(this.textBox_pathDB2_train.Text));
            trainingDataSet.setClassIndex(trainingDataSet.numAttributes() - 1);

            weka.core.Instances testingDataSet = new weka.core.Instances(new java.io.FileReader(this.textBox_pathDB2_test.Text));
            testingDataSet.setClassIndex(testingDataSet.numAttributes() - 1);

            сlassificationResults results = new сlassificationResults();
            Classifiers methodML = new Classifiers();

            results = methodML.RandomForest(trainingDataSet, testingDataSet, 10);
            PrintResults(results, Name_Exp);
          
        }

        public void C4_5()
        {
            string Name_Exp = this.textBox_Name_Exp_2.Text;
            weka.core.Instances trainingDataSet = new weka.core.Instances(new java.io.FileReader(this.textBox_pathDB2_train.Text));
            trainingDataSet.setClassIndex(trainingDataSet.numAttributes() - 1);

            weka.core.Instances testingDataSet = new weka.core.Instances(new java.io.FileReader(this.textBox_pathDB2_test.Text));
            testingDataSet.setClassIndex(testingDataSet.numAttributes() - 1);

            сlassificationResults results = new сlassificationResults();
            Classifiers methodML = new Classifiers();

            results = methodML.C4_5(trainingDataSet, testingDataSet);
            PrintResults(results, Name_Exp);


        }

        public void SVM()
        {
            string Name_Exp = this.textBox_Name_Exp_2.Text;
            weka.core.Instances trainingDataSet = new weka.core.Instances(new java.io.FileReader(this.textBox_pathDB2_train.Text));
            trainingDataSet.setClassIndex(trainingDataSet.numAttributes() - 1);

            weka.core.Instances testingDataSet = new weka.core.Instances(new java.io.FileReader(this.textBox_pathDB2_test.Text));
            testingDataSet.setClassIndex(testingDataSet.numAttributes() - 1);

            сlassificationResults results = new сlassificationResults();
            Classifiers methodML = new Classifiers();

            results = methodML.SVM(trainingDataSet, testingDataSet);
            PrintResults(results, Name_Exp);


        }



        public void PrintResults(сlassificationResults results, string Name_Exp)
        {
            string resultStr = "The name of the experiment " + Name_Exp + "\r\n";
            resultStr += "Algorithm Name: " + results.nameMetods + "\r\n \r\n  ";
            resultStr += results.summary + "\r\n\r\n ";
            resultStr += results.classDetails + "\r\n\r\n ";
            resultStr += "Training time: " + results.timeTraining + "  milliseconds\r\n ";
            resultStr += "Testing time: " + results.timeTesting + "  milliseconds\r\n ";
            resultStr += results.confusionMatrixString + "\r\n\r\n ";
            resultStr += "Normal       Anomaly \r\n ";
            resultStr += results.confusionMatrix[0][0] + "          " + results.confusionMatrix[0][1] + "\r\n ";
            resultStr += results.confusionMatrix[1][0] + "          " + results.confusionMatrix[1][1] + "\r\n \r\n ";
            resultStr += "\r\n \r\n ";
            AppendTextBox_2(resultStr);

      
        }


        

        //-----------------------Вкладка нечёткая логика-----------------------------------------------------------------------------

        private void button_add_pathDB_FL_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                this.textBox_pathDB_FL.Text = openFileDialog1.FileName;
            }
        }

        private void button_start_FL_Click(object sender, EventArgs e)
        {
            if (this.textBox_pathDB_FL.Text == "") { MessageBox.Show("Не была выбрана база данных!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error); }
            else
            {
                /* weka.core.converters.ConverterUtils.DataSource dataSource = new weka.core.converters.ConverterUtils.DataSource(this.textBox_pathDB_FL.Text);
                 weka.core.Instances instsDB = dataSource.getDataSet();
                 Classifiers methodML = new Classifiers();
                 textBox_resultOut_FL.Text = textBox_resultOut_FL.Text + methodML.Apriori(instsDB);
                 textBox_resultOut_FL.Text = textBox_resultOut_FL.Text + methodML.FPGrowth(instsDB);
                 */
                FuzzyLogic fuzzyLogic = new FuzzyLogic();
                textBox_resultOut_FL.Text += fuzzyLogic.Start(this.textBox_pathDB_FL.Text);
                
            }

        }

    }
}

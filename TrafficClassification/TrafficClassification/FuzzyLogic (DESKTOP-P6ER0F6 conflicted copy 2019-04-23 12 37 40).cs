﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AI.Fuzzy.Library;

namespace TrafficClassification
{
    class FuzzyLogic
    {
        SugenoFuzzySystem _fsCruiseControl = null;


        SugenoFuzzySystem CreateSystem()
        {
            //
            // Create empty Sugeno Fuzzy System
            //
            SugenoFuzzySystem fsCruiseControl = new SugenoFuzzySystem();

            //
            // Create input variables for the system
            //
            FuzzyVariable fv_src_bytes = new FuzzyVariable("src_bytes", 0, 10);
            fv_src_bytes.Terms.Add(new FuzzyTerm("VL", new TriangularMembershipFunction(0, 0.003, 0.007)));
            fv_src_bytes.Terms.Add(new FuzzyTerm("L", new TriangularMembershipFunction(0.005, 0.03, 0.06)));
            fv_src_bytes.Terms.Add(new FuzzyTerm("M", new TriangularMembershipFunction(0.05, 0.2, 0.4)));
            fv_src_bytes.Terms.Add(new FuzzyTerm("H", new TriangularMembershipFunction(0.3, 5.0, 10)));
            fsCruiseControl.Input.Add(fv_src_bytes);

            FuzzyVariable fv_dst_bytes = new FuzzyVariable("dst_bytes", 0, 10);
            fv_dst_bytes.Terms.Add(new FuzzyTerm("VL", new TriangularMembershipFunction(0, 0.003, 0.007)));
            fv_dst_bytes.Terms.Add(new FuzzyTerm("L", new TriangularMembershipFunction(0.005, 0.03, 0.06)));
            fv_dst_bytes.Terms.Add(new FuzzyTerm("M", new TriangularMembershipFunction(0.05, 0.2, 0.4)));
            fv_dst_bytes.Terms.Add(new FuzzyTerm("H", new TriangularMembershipFunction(0.3, 5.0, 10)));
            fsCruiseControl.Input.Add(fv_dst_bytes);

            FuzzyVariable fv_src_bytes = new FuzzyVariable("src_bytes", 0, 10);
            fv_src_bytes.Terms.Add(new FuzzyTerm("VL", new TriangularMembershipFunction(0, 0.003, 0.007)));
            fv_src_bytes.Terms.Add(new FuzzyTerm("L", new TriangularMembershipFunction(0.005, 0.03, 0.06)));
            fv_src_bytes.Terms.Add(new FuzzyTerm("M", new TriangularMembershipFunction(0.05, 0.2, 0.4)));
            fv_src_bytes.Terms.Add(new FuzzyTerm("H", new TriangularMembershipFunction(0.3, 5.0, 10)));
            fsCruiseControl.Input.Add(fv_src_bytes);

            FuzzyVariable fv_src_bytes = new FuzzyVariable("src_bytes", 0, 10);
            fv_src_bytes.Terms.Add(new FuzzyTerm("VL", new TriangularMembershipFunction(0, 0.003, 0.007)));
            fv_src_bytes.Terms.Add(new FuzzyTerm("L", new TriangularMembershipFunction(0.005, 0.03, 0.06)));
            fv_src_bytes.Terms.Add(new FuzzyTerm("M", new TriangularMembershipFunction(0.05, 0.2, 0.4)));
            fv_src_bytes.Terms.Add(new FuzzyTerm("H", new TriangularMembershipFunction(0.3, 5.0, 10)));
            fsCruiseControl.Input.Add(fv_src_bytes);

            FuzzyVariable fv_src_bytes = new FuzzyVariable("src_bytes", 0, 10);
            fv_src_bytes.Terms.Add(new FuzzyTerm("VL", new TriangularMembershipFunction(0, 0.003, 0.007)));
            fv_src_bytes.Terms.Add(new FuzzyTerm("L", new TriangularMembershipFunction(0.005, 0.03, 0.06)));
            fv_src_bytes.Terms.Add(new FuzzyTerm("M", new TriangularMembershipFunction(0.05, 0.2, 0.4)));
            fv_src_bytes.Terms.Add(new FuzzyTerm("H", new TriangularMembershipFunction(0.3, 5.0, 10)));
            fsCruiseControl.Input.Add(fv_src_bytes);


            //
            // Create output variables for the system
            //
            SugenoVariable svAccelerate = new SugenoVariable("Accelerate");
            svAccelerate.Functions.Add(fsCruiseControl.CreateSugenoFunction("zero", new double[] { 0.0, 0.0, 0.0 }));
            svAccelerate.Functions.Add(fsCruiseControl.CreateSugenoFunction("faster", new double[] { 0.0, 0.0, 1.0 }));
            svAccelerate.Functions.Add(fsCruiseControl.CreateSugenoFunction("slower", new double[] { 0.0, 0.0, -1.0 }));
            svAccelerate.Functions.Add(fsCruiseControl.CreateSugenoFunction("func", new double[] { -0.04, -0.1, 0.0 }));
            fsCruiseControl.Output.Add(svAccelerate);

            //
            // Create fuzzy rules
            //
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "slower", "slower", "faster");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "slower", "zero", "faster");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "slower", "faster", "zero");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "zero", "slower", "faster");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "zero", "zero", "func");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "zero", "faster", "slower");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "faster", "slower", "zero");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "faster", "zero", "slower");
            AddSugenoFuzzyRule(fsCruiseControl, fvSpeedError, fvSpeedErrorDot, svAccelerate, "faster", "faster", "slower");

            //
            // Adding the same rules in text form
            //
            ///////////////////////////////////////////////////////////////////
            //SugenoFuzzyRule rule1 = fsCruiseControl.ParseRule("if (SpeedError is slower) and (SpeedErrorDot is slower) then (Accelerate is faster)");
            //SugenoFuzzyRule rule2 = fsCruiseControl.ParseRule("if (SpeedError is slower) and (SpeedErrorDot is zero) then (Accelerate is faster)");
            //SugenoFuzzyRule rule3 = fsCruiseControl.ParseRule("if (SpeedError is slower) and (SpeedErrorDot is faster) then (Accelerate is zero)");
            //SugenoFuzzyRule rule4 = fsCruiseControl.ParseRule("if (SpeedError is zero) and (SpeedErrorDot is slower) then (Accelerate is faster)");
            //SugenoFuzzyRule rule5 = fsCruiseControl.ParseRule("if (SpeedError is zero) and (SpeedErrorDot is zero) then (Accelerate is func)");
            //SugenoFuzzyRule rule6 = fsCruiseControl.ParseRule("if (SpeedError is zero) and (SpeedErrorDot is faster) then (Accelerate is slower)");
            //SugenoFuzzyRule rule7 = fsCruiseControl.ParseRule("if (SpeedError is faster) and (SpeedErrorDot is slower) then (Accelerate is faster)");
            //SugenoFuzzyRule rule8 = fsCruiseControl.ParseRule("if (SpeedError is faster) and (SpeedErrorDot is zero) then (Accelerate is slower)");
            //SugenoFuzzyRule rule9 = fsCruiseControl.ParseRule("if (SpeedError is faster) and (SpeedErrorDot is faster) then (Accelerate is slower)");

            //fsCruiseControl.Rules.Add(rule1);
            //fsCruiseControl.Rules.Add(rule2);
            //fsCruiseControl.Rules.Add(rule3);
            //fsCruiseControl.Rules.Add(rule4);
            //fsCruiseControl.Rules.Add(rule5);
            //fsCruiseControl.Rules.Add(rule6);
            //fsCruiseControl.Rules.Add(rule7);
            //fsCruiseControl.Rules.Add(rule8);
            //fsCruiseControl.Rules.Add(rule9);

            ///////////////////////////////////////////////////////////////////

            return fsCruiseControl;
        }

        void AddSugenoFuzzyRule(
            SugenoFuzzySystem fs,
            FuzzyVariable fv1,
            FuzzyVariable fv2,
            SugenoVariable sv,
            string value1,
            string value2,
            string result)
        {
            SugenoFuzzyRule rule = fs.EmptyRule();
            rule.Condition.Op = OperatorType.And;
            rule.Condition.ConditionsList.Add(rule.CreateCondition(fv1, fv1.GetTermByName(value1)));
            rule.Condition.ConditionsList.Add(rule.CreateCondition(fv2, fv2.GetTermByName(value2)));
            rule.Conclusion.Var = sv;
            rule.Conclusion.Term = sv.GetFuncByName(result);
            fs.Rules.Add(rule);
        }
        
        public string Start(string path)
        {
            string result_str = "";
            StreamReader sr = new StreamReader(path);
            string line= "";
            string attribute = "";
            int numb_atr = -2;
            while ((line = sr.ReadLine()) != "@data")
            {
                attribute += line + "\r\n";
                numb_atr++;
            }

            bool haveAttribute = true;
            if (attribute.IndexOf("src_bytes") < 0) haveAttribute = false;
            if (attribute.IndexOf("dst_bytes") < 0) haveAttribute = false;
            if (attribute.IndexOf("serror_rate") < 0) haveAttribute = false;
            if (attribute.IndexOf("dst_host_serror_rate") < 0) haveAttribute = false;
            if (attribute.IndexOf("dst_host_srv_serror_rate") < 0) haveAttribute = false;
            

            if (haveAttribute == false)
            {
                result_str = "Ошибка! В БД нет необходимых признаков.";
            }
            else
            {
                //
                // Create new fuzzy system
                //
                if (_fsCruiseControl == null)
                {
                    _fsCruiseControl = CreateSystem();
                    if (_fsCruiseControl == null)
                    {
                        result_str = "Ошибка при создании нечёткой системы";
                        return result_str;
                    }
                }

                //
                // Create new fuzzy system
                //


                string[] atr_value = new string[numb_atr];
                while ((line = sr.ReadLine()) != null)
                {
                    atr_value = line.Split(',');
                }



               
                //
                // Get variables from the system (for convinience only)
                //
                FuzzyVariable fvSpeedError = _fsCruiseControl.InputByName("SpeedError");
                FuzzyVariable fvSpeedErrorDot = _fsCruiseControl.InputByName("SpeedErrorDot");
                SugenoVariable svAccelerate = _fsCruiseControl.OutputByName("Accelerate");

                //
                // Fuzzify input values
                //
                Dictionary<FuzzyVariable, double> inputValues = new Dictionary<FuzzyVariable, double>();
                //inputValues.Add(fvSpeedError, (double)nudInputSpeedError.Value); // атрибуты
                //inputValues.Add(fvSpeedErrorDot, (double)nudInputSpeedErrorDot.Value);

                //
                // Calculate the result
                //
                Dictionary<SugenoVariable, double> result = _fsCruiseControl.Calculate(inputValues);

                result_str = (result[svAccelerate] * 100.0).ToString("f1");
                
            }
            return result_str;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Threading;

namespace TrafficClassification
{
    class Interaction_with_DB:Form1
    {
        public string connectionString = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=D:\TortoiseHg\traffic_classification_c\Сергей\TrafficClassification\Database1.mdf;Integrated Security=True";
        static public string[] attributeMas = new string[]
               {"@attribute 'duration' real",
                "@attribute 'protocol_type' {'tcp','udp', 'icmp'}",
                "@attribute 'service' {'aol', 'auth', 'bgp', 'courier', 'csnet_ns', 'ctf', 'daytime', 'discard', 'domain', 'domain_u', 'echo', 'eco_i', 'ecr_i', 'efs', 'exec', 'finger', 'ftp', 'ftp_data', 'gopher', 'harvest', 'hostnames', 'http', 'http_2784', 'http_443', 'http_8001', 'imap4', 'IRC', 'iso_tsap', 'klogin', 'kshell', 'ldap', 'link', 'login', 'mtp', 'name', 'netbios_dgm', 'netbios_ns', 'netbios_ssn', 'netstat', 'nnsp', 'nntp', 'ntp_u', 'other', 'pm_dump', 'pop_2', 'pop_3', 'printer', 'private', 'red_i', 'remote_job', 'rje', 'shell', 'smtp', 'sql_net', 'ssh', 'sunrpc', 'supdup', 'systat', 'telnet', 'tftp_u', 'tim_i', 'time', 'urh_i', 'urp_i', 'uucp', 'uucp_path', 'vmnet', 'whois', 'X11', 'Z39_50'}",
                "@attribute 'flag' { 'OTH', 'REJ', 'RSTO', 'RSTOS0', 'RSTR', 'S0', 'S1', 'S2', 'S3', 'SF', 'SH' }",
                "@attribute 'src_bytes' real",
                "@attribute 'dst_bytes' real",
                "@attribute 'land' {'0', '1'}",
                "@attribute 'wrong_fragment' real",
                "@attribute 'urgent' real",
                "@attribute 'hot' real",
                "@attribute 'num_failed_logins' real",
                "@attribute 'logged_in' {'0', '1'}",
                "@attribute 'num_compromised' real",
                "@attribute 'root_shell' real",
                "@attribute 'su_attempted' real",
                "@attribute 'num_root' real",
                "@attribute 'num_file_creations' real",
                "@attribute 'num_shells' real",
                "@attribute 'num_access_files' real",
                "@attribute 'num_outbound_cmds' real",
                "@attribute 'is_host_login' {'0', '1'}",
                "@attribute 'is_guest_login' {'0', '1'}",
                "@attribute 'count' real",
                "@attribute 'srv_count' real",
                "@attribute 'serror_rate' real",
                "@attribute 'srv_serror_rate' real",
                "@attribute 'rerror_rate' real",
                "@attribute 'srv_rerror_rate' real",
                "@attribute 'same_srv_rate' real",
                "@attribute 'diff_srv_rate' real",
                "@attribute 'srv_diff_host_rate' real",
                "@attribute 'dst_host_count' real",
                "@attribute 'dst_host_srv_count' real",
                "@attribute 'dst_host_same_srv_rate' real",
                "@attribute 'dst_host_diff_srv_rate' real",
                "@attribute 'dst_host_same_src_port_rate' real",
                "@attribute 'dst_host_srv_diff_host_rate' real",
                "@attribute 'dst_host_serror_rate' real",
                "@attribute 'dst_host_srv_serror_rate' real",
                "@attribute 'dst_host_rerror_rate' real",
                "@attribute 'dst_host_srv_rerror_rate' real",
                "@attribute 'class_normal_anomaly' {'normal', 'anomaly'}",
                "@attribute 'class_normal_DOS' {'normal', 'DOS'}",
                "@attribute 'class_normal_U2R' {'normal', 'U2R'}",
                "@attribute 'class_normal_R2L' {'normal', 'R2L'}",
                "@attribute 'class_normal_probe' {'normal', 'probe'}",
                "@attribute 'class_normal_all_attacks' {'normal', 'neptune', 'normal', 'saint', 'mscan', 'guess_passwd', 'smurf', 'apache2', 'satan', 'buffer_overflow', 'back', 'warezmaster', 'snmpgetattack', 'processtable', 'pod', 'httptunnel', 'nmap', 'ps', 'snmpguess', 'ipsweep', 'mailbomb', 'portsweep', 'multihop', 'named', 'sendmail', 'loadmodule', 'xterm', 'worm', 'teardrop', 'rootkit', 'xlock', 'perl', 'land', 'xsnoop', 'sqlattack', 'ftp_write', 'imap', 'udpstorm', 'phf', 'warezclient', 'spy'}",
                "@attribute 'class' {'normal', 'DOS', 'U2R', 'R2L', 'probe'}",
        };

        public void create_1_DB(int type_classification, List<int> featuresMas, int numbTraining, string nameDB)
        {
            string attributeStr = "";

            for (int i = 0; i < featuresMas.Count; i++) attributeStr += attributeMas[featuresMas[i] - 1] + " \r\n";
            attributeStr += attributeMas[41 + type_classification] + " \r\n";
            
            string str_folder = Directory.GetCurrentDirectory() + "\\Наборы данных\\" + nameDB;
            

            Create_ARFF(type_classification, featuresMas, 0, 148517, nameDB, attributeStr, str_folder);
            
            
        }

        public void create_2_DB(int type_classification, List<int> featuresMas, int numbTraining, string nameDB)
        {
            string attributeStr = "";

           

            for (int i = 0; i < featuresMas.Count; i++) attributeStr += attributeMas[featuresMas[i] - 1] + " \r\n";
            attributeStr += attributeMas[41 + type_classification] + " \r\n";
           
            string str_folder = Directory.GetCurrentDirectory() + "\\Наборы данных\\" + nameDB;
           

            Create_ARFF(type_classification, featuresMas, 0, numbTraining, nameDB + "_Training", attributeStr, str_folder);
           
            Create_ARFF(type_classification, featuresMas, numbTraining + 1, 148517, nameDB + "_Test", attributeStr, str_folder);
          
            
        }

       

        private void Create_ARFF(int type_classification, List<int> featuresMas, int first, int last, string nameDB, string attribute, string str_folder)
        {

            System.IO.Directory.CreateDirectory(str_folder);
            
            string temp_out_str = "";
            temp_out_str = "@relation '" + nameDB + "' \n";
            temp_out_str += attribute;
            temp_out_str += "@data\n";

            string path = str_folder + "\\"+ nameDB + ".arff";
            
            try
            {                
                StreamWriter fs = new  StreamWriter(path, true);
                fs.Write(temp_out_str);
                
                int counter = 0;
                string line ="";
                string[] temp_mas = new string[48];
                string temp_str = "";
                string temp = "";
                System.IO.StreamReader file = new System.IO.StreamReader(Directory.GetCurrentDirectory() + "\\NSL-KDD.txt");
     
                while ((line = file.ReadLine()) != null)
                {
                    counter++;
                    if (counter >= first && counter <= last)
                        {
                            line = line.Replace(", ", ",");
                            temp_mas = line.Split(',');


                            for (int i = 0; i < (featuresMas.Count); i++)
                            {
                                temp = temp_mas[featuresMas[i] - 1].ToString();
                                if (temp.IndexOf(',') > -1) temp = temp.Replace(',', '.');
                                temp_str += temp + ",";
                            }
                            temp_str += temp_mas[41 + type_classification].ToString() + "\r\n";
                            fs.Write(temp_str);
                            temp_str = "";
                        }
                   
                }
                fs.Close();
                file.Close();

            }

            catch (Exception ex)
            {
                string error = ex.ToString();
            }

        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using weka.classifiers.evaluation;
using weka.core;

namespace TrafficClassification
{
    public struct сlassificationResults
    {
        public string nameMetods;
        public string summary;
        public string classDetails;
        public double[][] confusionMatrix;
        public string confusionMatrixString;
        public long timeTraining;
        public long timeTesting;
        public  string exception;
    }

    class Classifiers
    {


        public сlassificationResults RandomForest(weka.core.Instances trainingDataSet, weka.core.Instances testingDataSet, int setNumIteration)
        {
            сlassificationResults results = new сlassificationResults();

            try
            {
                weka.classifiers.trees.RandomForest forest = new weka.classifiers.trees.RandomForest();
                forest.setNumIterations(setNumIteration);

                System.Diagnostics.Stopwatch myStopwatch = new System.Diagnostics.Stopwatch();
                myStopwatch.Start(); //запуск
                forest.buildClassifier(trainingDataSet);
                myStopwatch.Stop(); //остановить
                results.timeTraining = myStopwatch.ElapsedMilliseconds;

                weka.classifiers.Evaluation eval = new weka.classifiers.Evaluation(trainingDataSet);
                myStopwatch.Restart();
                eval.evaluateModel(forest, testingDataSet);
                myStopwatch.Stop(); //остановить
                results.timeTesting = myStopwatch.ElapsedMilliseconds;

                //results.nameMetods = forest.ToString();
                results.nameMetods = "Random Forest";
                results.summary = "** Decision Tress Evaluation with Datasets ** \r\n" + eval.toSummaryString();
                results.classDetails = eval.toClassDetailsString();
                results.confusionMatrix = eval.confusionMatrix();
                results.confusionMatrixString = eval.toMatrixString();
                results.exception = "";

                ThresholdCurve tc = new ThresholdCurve();
                Instances resultCurve = tc.getCurve(eval.predictions());
                double ROCArea = ThresholdCurve.getROCArea(resultCurve);

            }
            catch (Exception ex)
            {
                double[][] nullMatrix = new double[2][];
                nullMatrix[0] = new double[] { 0, 0 };
                nullMatrix[1] = new double[] { 0, 0 };
                results.nameMetods = "Random Forest";
                results.summary = "";
                results.confusionMatrix = nullMatrix;
                results.confusionMatrixString = "";
                results.exception = ex.Message;
            }
            return results;
        }

        public сlassificationResults C4_5(weka.core.Instances trainingDataSet, weka.core.Instances testingDataSet)
        {
            сlassificationResults results = new сlassificationResults();

            try
            {
                weka.classifiers.Classifier forest = new weka.classifiers.trees.J48();
                System.Diagnostics.Stopwatch myStopwatch = new System.Diagnostics.Stopwatch();
                myStopwatch.Start(); //запуск
                forest.buildClassifier(trainingDataSet);
                myStopwatch.Stop(); //остановить
                results.timeTraining = myStopwatch.ElapsedMilliseconds;

                weka.classifiers.Evaluation eval = new weka.classifiers.Evaluation(trainingDataSet);
                myStopwatch.Restart();
                eval.evaluateModel(forest, testingDataSet);
                myStopwatch.Stop(); //остановить
                //var predictions = eval.predictions();
                results.timeTesting = myStopwatch.ElapsedMilliseconds;


                //results.nameMetods = forest.ToString();
                results.nameMetods = "C4.5";
                results.summary = "** Decision Tress Evaluation with Datasets ** \r\n" + eval.toSummaryString();
                results.classDetails = eval.toClassDetailsString();
                results.confusionMatrix = eval.confusionMatrix();
                results.confusionMatrixString = eval.toMatrixString();
                results.exception = "";
            }
            catch (Exception ex)
            {
                double[][] nullMatrix = new double[2][];
                nullMatrix[0] = new double[] { 0, 0 };
                nullMatrix[1] = new double[] { 0, 0 };
                results.nameMetods = "C4.5";
                results.summary = "";
                results.confusionMatrix = nullMatrix;
                results.confusionMatrixString = "";
                results.exception = ex.Message;
            }
            return results;
        }

        public сlassificationResults SVM(weka.core.Instances trainingDataSet, weka.core.Instances testingDataSet)
        {
            сlassificationResults results = new сlassificationResults();

            try
            {
                weka.classifiers.functions.SMO SVM = new weka.classifiers.functions.SMO();

                System.Diagnostics.Stopwatch myStopwatch = new System.Diagnostics.Stopwatch();
                myStopwatch.Start(); //запуск
                SVM.buildClassifier(trainingDataSet);
                myStopwatch.Stop(); //остановить
                results.timeTraining = myStopwatch.ElapsedMilliseconds;

                weka.classifiers.Evaluation eval = new weka.classifiers.Evaluation(trainingDataSet);
                myStopwatch.Restart();
                eval.evaluateModel(SVM, testingDataSet);
                myStopwatch.Stop(); //остановить
                results.timeTesting = myStopwatch.ElapsedMilliseconds;


                //results.nameMetods = SVM.ToString();
                results.nameMetods = "SVM";
                results.summary = "** Decision Tress Evaluation with Datasets ** \r\n" + eval.toSummaryString();
                results.classDetails = eval.toClassDetailsString();
                results.confusionMatrix = eval.confusionMatrix();
                results.confusionMatrixString = eval.toMatrixString();
                results.exception = "";

            }
            catch (Exception ex)
            {
                double[][] nullMatrix = new double[2][];
                nullMatrix[0] = new double[] { 0, 0 };
                nullMatrix[1] = new double[] { 0, 0 };
                results.nameMetods = "SVM";
                results.summary = "";
                results.confusionMatrix = nullMatrix;
                results.confusionMatrixString = "";
                results.exception = ex.Message;
            }
            return results;
        }

        public string Apriori(weka.core.Instances DataSet)
        {

            string result = "";
            try
            {
                weka.associations.Apriori Apriori = new weka.associations.Apriori();
                Apriori.buildAssociations(DataSet); //NumericToNominal(DataSet, "1"));
                result += "Number of Associations : " + Apriori.getNumRules() + "\r\n";
                result += "Adding Association Information to ArrayList .. \r\n";
                int num = Apriori.getAllTheRules().Length;
                result += "Num :" + num;
                foreach (FastVector fastVector in Apriori.getAllTheRules()) { result += "rules " + fastVector.toArray(); }
                // weka.associations.AprioriItemSet premise = (weka.associations.AprioriItemSet)m_allTheRules[0].elementAt(i);
                //  weka.associations.AprioriItemSet consequence = (weka.associations.AprioriItemSet)m_allTheRules[1].elementAt(i);
                result += Apriori.ToString();

            }
            catch (Exception ex)
            {
                result = ex.ToString();
            }
            return result;
        }

        public string FPGrowth(weka.core.Instances DataSet)
        {
            string result = "";

            try
            {
                weka.associations.FPGrowth FPGrowth = new weka.associations.FPGrowth();
                FPGrowth.buildAssociations(DataSet);
                /*
                                weka.associations.RuleGeneration ruleGeneration = new weka.associations.RuleGeneration(DataSet);
                                ruleGeneration.generateRules();

                    */
                FPGrowth.ToString();

            }
            catch (Exception ex)
            {
                result = ex.ToString();
            }
            return result;
        }

        public static Instances NumericToNominal(Instances dataProcessed, string variables)
        { 
            weka.filters.unsupervised.attribute.NumericToNominal convert = new weka.filters.unsupervised.attribute.NumericToNominal();
		    string[] options = new string[2];
            options[0] = "-R";
		    options[1] = variables;
		    convert.setOptions(options);
		    convert.setInputFormat(dataProcessed);
		    Instances filterData = weka.filters.Filter.useFilter(dataProcessed, convert);
		    return filterData;
        }
    }
       
    
}

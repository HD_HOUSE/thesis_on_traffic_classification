﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Interop.Excel;
namespace txtToExcell
{
    class Program
    {
        static void Main(string[] args)
        {
            //Объявляем приложение
            Microsoft.Office.Interop.Excel.Application ex = new Microsoft.Office.Interop.Excel.Application();
            //Отобразить Excel
            ex.Visible = true;
            //Количество листов в рабочей книге
            ex.SheetsInNewWorkbook = 1;
            //Добавить рабочую книгу
            Microsoft.Office.Interop.Excel.Workbook workBook = ex.Workbooks.Add(Type.Missing);
            //Получаем первый лист документа (счет начинается с 1)
            Microsoft.Office.Interop.Excel.Worksheet sheet = (Microsoft.Office.Interop.Excel.Worksheet)ex.Worksheets.get_Item(1);
            //Название листа (вкладки снизу)
            sheet.Name = "Лист 1";

            int count = 0;
            int sovpad = 0;
            int sovpad_1 = 0;
            int sovpad_2 = 0;
            int sovpad_3 = 0;
            string line;
            string[] str_mas = new string[8];
            StreamReader sr = new StreamReader(@"D:\Clouds\Dropbox\Учёба\Диссертация\Отчёты по классификации\Нечёткая логика.txt");
            while ((line = sr.ReadLine()) != null)
            {
                line = line.Replace(" ", "");
                str_mas = line.Split(',');
                count++;
                //Заполнение ячеек строки
                // for (int j = 1; j < 9; j++) sheet.Cells[count, j] = str_mas[j - 1].ToString();

                if (str_mas[6] == str_mas[7]) { sovpad++; sovpad_1++; sovpad_2++; sovpad_3++; }
                if (str_mas[6] == "normal" && str_mas[7] == "mb_normal") { sovpad_1++;  sovpad_3++; }
                if (str_mas[6] == "anomaly" && str_mas[7] == "mb_anomaly") { sovpad_1++; sovpad_2++; }
                if (str_mas[6] == "anomaly" && str_mas[7] == "mb_normal") { sovpad_2++; }
                if (str_mas[6] == "normal" && str_mas[7] == "mb_anomaly") { sovpad_3++; }
            }
            double proc = (Convert.ToDouble(sovpad) / (count - 1)) * 100;
            double proc_1 = (Convert.ToDouble(sovpad_1) / (count - 1)) * 100;
            double proc_2 = (Convert.ToDouble(sovpad_2) / (count - 1)) * 100;
            double proc_3 = (Convert.ToDouble(sovpad_3) / (count - 1)) * 100;

            sheet.Cells[1, 10] = "Всего записей";
            sheet.Cells[2, 10] = count - 1;
            sheet.Cells[1, 11] = "Правильно классифицированных записей";
            sheet.Cells[2, 11] = sovpad;
            sheet.Cells[3, 11] = sovpad_1;
            sheet.Cells[4, 11] = sovpad_2;
            sheet.Cells[5, 11] = sovpad_3;
            sheet.Cells[1, 12] = "%";
            sheet.Cells[2, 12] = proc;
            sheet.Cells[3, 12] = proc_1;
            sheet.Cells[4, 12] = proc_2;
            sheet.Cells[5, 12] = proc_3;
            sheet.Cells[1, 13] = "Описание";
            sheet.Cells[2, 13] = "Учитывается только абсолютное совпадение с меткой в БД";
            sheet.Cells[3, 13] = "mb_normal относится к нормальным даным, а mb_ anomaly к аномалии";
            sheet.Cells[4, 13] = "mb_normal и mb_ anomaly относятся к аномалии";
            sheet.Cells[5, 13] = "mb_normal и mb_ anomaly относятся к нормальным данным";
        }
    }
}

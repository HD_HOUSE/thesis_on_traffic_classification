﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enumeration_of_combinations
{
    class Program
    {
        static void Main(string[] args)
        {

            int[] mas = new int[5] { 0, 0, 0, 0, 0 };
            int count = 0;
            Console.Write("1 or 2: ");

           string type = Console.ReadLine();

            while ((mas[0] != 3) && (mas[1] != 3) && (mas[2] != 3) && (mas[3] != 3) && (mas[4] != 3))
            {

                if (mas[4] == 3) { mas[4] = 0; mas[3]++; }
                else mas[4]++;

                
                int[] vocabulary = new int[4] { 0, 1, 2, 3, };
                                     
                for (int r = 0; r < vocabulary.Length; r++)
                {
                     mas[0] = vocabulary[r];
                     for (int e = 0; e < vocabulary.Length; e++)
                     {
                        mas[1] = vocabulary[e];
                        for (int w = 0; w < vocabulary.Length; w++)
                        {
                            mas[2] = vocabulary[w];
                            for (int q = 0; q < vocabulary.Length; q++)
                            {
                                mas[3] = vocabulary[q];
                                for (int j = 0; j < vocabulary.Length; j++)
                                {
                                    mas[4] = vocabulary[j];
                                    count++;

                                    switch(type)
                                    {
                                        case "1":
                                            Console.Write("MamdaniFuzzyRule rule" + count + " = fsMamdani.ParseRule(\"if " +
                                                "(Src_bytes is " + ToLinguistic(mas[0]) + ") and " +
                                                "(Dst_bytes is " + ToLinguistic(mas[1]) + ") and " +
                                                "(Serror_rate is " + ToLinguistic(mas[2]) + ") and " +
                                                "(Dst_host_serror_rate is " + ToLinguistic(mas[3]) + ") and " +
                                                "(Dst_host_srv_serror_rate is " + ToLinguistic(mas[4]) + ")" +
                                                " then Attack is " + Output_var(mas) + "\"); fsMamdani.Rules.Add(rule" + count + ");  //" + count + "\r\n");
                                        break;

                                        case "2":
                                            Console.Write("AddSugenoFuzzyRule(fsSugeno, fvSrc_bytes, fvDst_bytes, fvSerror_rate, fvDst_host_serror_rate, fvDst_host_srv_serror_rate, svAttack, ");
                                            Console.Write('"' + ToLinguistic(mas[0]) + '"' + ", " + '"' + ToLinguistic(mas[1]) + '"' + ", " + '"' + ToLinguistic(mas[2]) + '"' + ", " + '"' + ToLinguistic(mas[3]) + '"' + ", " + '"' + ToLinguistic(mas[4]) + '"' + ", ");
                                            Console.Write('"' + Output_var(mas) + '"' + "); //" + count + "\r\n");
                                        break;

                                        default:
                                            break;
                                    }
                                }
                            }
                        }
                     }
                }

               

            }
            Console.ReadLine();

        }

        static string ToLinguistic(int temp)
        {
            string output = "";
            switch (temp)
            {
                case 0:
                    output = "VL";
                    break;
                case 1:
                    output = "L";
                    break;
                case 2:
                    output = "M";
                    break;
                case 3:
                    output = "H";
                    break;
                default:
                    output = "Error";
                    break;                    
            }
            return output;
        }

        static string Output_var(int[] mas)
        {
            string output = "";
            
            int[] linguistic = new int[4] { 0, 0, 0, 0 };
            for (int i = 0; i < mas.Length; i++)
            {
                switch (mas[i])
                {
                    case 0:
                        linguistic[0]++;
                        break;
                    case 1:
                        linguistic[1]++;
                        break;
                    case 2:
                        linguistic[2]++;
                        break;
                    case 3:
                        linguistic[3]++;
                        break;
                    default:
                        break;
                }
            }
            int index = Array.IndexOf(linguistic, linguistic.Max());

            switch (index)
            {
                case 0:
                    output = "normal";
                    break;
                case 1:
                    output = "mb_normal";
                    break;
                case 2:
                    output = "mb_attack";
                    break;
                case 3:
                    output = "attack";
                    break;
                default:
                    output = "Error";
                    break;
            }

            return output;
        }
    }
}
